# Paris Fire Brigade Challenge

## About this project

This is the code and experiments developped for the [Paris Fire Brigade Challenge](https://paris-fire-brigade.github.io/data-challenge/challenge.html)

## Structure of the project

The project is organised as follows:

| File or folder        | Role                                                |
| --------------------- | --------------------------------------------------- |
| `code/`               | Our code base (detailed in the section bellow).     |
| `notebooks/`          | Jupyter Notebooks for experimenting things          |
| `misc/`               | Various files, OSRM profiles developped             |
| `README.md`           | This file.                                          |
| `report/`             | LaTeX report (written in French).                   |
| `requirements.txt`    | Python packages.                                    |
| `slides/`             | Beamer presentation                                 |

## Notebooks

Experiments have been made in Jupyter Notebooks, here is a short description

| File or folder               | Role                                                |
| ---------------------------- | --------------------------------------------------- |
| `explo_data.ipynb`           | Exploration of the original dataset and assumptions |
| `etude_representation.ipynb` | Exploration of the final dataset                    |
| `delta_selection.ipynb`      | Analysis for the delta_selection-departure variable |
| `osrm.ipynb`                 | Exploration of new OSRM itinary using new profiles  |
| `traffic.ipynb`              | Exploration of the traffic data                     |


## Structure of the code base : `code`

The code base is organised as follows: 

| File                   | Role                                                         |
| ---------------------- | ------------------------------------------------------------ |
| `settings.py`          | All the settings used for the project.                       |
| `extract_osrm.py`      | Query servers to get latest OSRM estimations                 |
| `hp_optim.py`          | Hyperparameters search                                       |
| `interfaces.py `       | Various interfaces to interact with data on disks            |
| `interpret_model.py`   | Experiments to interpret models                              |
| `meteo.py`             | Experiments with weather data                                |
| `preprocessing.py`     | Create the data set to learn models then                     |
| `settings.py`          | Various settings                                             |
| `submit_one_model.py`  | Create CSV submissions for one model only                    |
| `submit_two_model.py`  | Create CSV submissions for two models                        |
| `submit_three_model.py`| Create CSV submissions for three models                      |
| `train_lightgbm.py`    | Train LightGBM booster                                       |
| `train_neuralnet.py`   | Experiment with Multilayers Perceptron                       |
| `train_simpler_models.py`| Train Random Forets, Linear Regression                     |
| `utils.py`             | Various functions                                            |

## Using scripts

Three main steps:
 - run `preprocessing.py` to create the dataset
 - run `train_lightgbm.py` (or `train_simpler_model.py` to learn a model
    - the model will be saved in a session folder
 - run `submit_one_model.py`, `submit_two_models.py` or `submit_three _model.py` to create submission for models 

## About the license

This project is accessible under GPL-3.0.

