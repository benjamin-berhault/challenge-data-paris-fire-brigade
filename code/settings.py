#!/usr/bin/env python
# coding: utf-8

from pathlib import Path

from joblib import Memory
from sklearn.metrics import r2_score

# FOLDERS
ROOT = Path(__file__).parents[1]
CACHE = Memory(ROOT / "cache", verbose=0)

# Data given
DATA_FOLDER = ROOT / "data"
DEPARTEMENT_DATA = DATA_FOLDER / "geojson"
TRAINING_DATA = DATA_FOLDER / "train"
TESTING_DATA = DATA_FOLDER / "test"
TRAFFIC_DATA = DATA_FOLDER / "traffic"
TRANSFORMED_DATA = DATA_FOLDER / "transformed"

SUBMISSION_FOLDER = ROOT / "soumissions"
SESSIONS_FOLDER = ROOT / "sessions"

# Base datasets
Y_TRAIN = TRAINING_DATA / "y_train.csv"

X_TRAIN = TRAINING_DATA / "x_train.csv"
X_TRAIN_ADD = TRAINING_DATA / "x_train_additional_file.csv"

X_TEST = TESTING_DATA / "x_test.csv"
X_TEST_ADD = TESTING_DATA / "x_test_additional_file.csv"

OSRM_ESTIMATES_CSV = DATA_FOLDER / "osrm.csv"

# Evaluation settings
METRICS_FOR_EVALUATION = [r2_score]

INDEX_COLUMN = 'emergency vehicle selection'

delta_preparation = 'delta selection-departure'
delta_transit = 'delta departure-presentation'
delta_total = 'delta selection-presentation'

TO_PREDICT = [delta_preparation, delta_transit, delta_total]

DATE_FORMAT_DATASET = "%Y-%m-%d %H:%M.f"

# Number of observation on the test set
n_test = 108033

# Number of observation on the train set
n_train = 219337

# The local data base persisting the data
LOCAL_DATABASE = DATA_FOLDER / "traffic.db"

CREDENTIALS_FILE = DATA_FOLDER / ".credentials"
DB_URL_FILE = DATA_FOLDER / ".db_url"

alert_reason_category_name = {
    "alert_reason_category_1": "raison_feu",
    "alert_reason_category_2": "raison_accidents_circulation",
    "alert_reason_category_3": "raison_secours_victime",
    "alert_reason_category_4": "raison_assistance_personne",
    "alert_reason_category_5": "raison_fait_animaux",
    "alert_reason_category_6": "raison_eau_gaz_electricite",
    "alert_reason_category_7": "raison_autres",
    "alert_reason_category_8": "raison_radiologique_biologique_etc",
    "alert_reason_category_9": "raison_reconnaissance_recherche"
}
