#!/usr/bin/env python
# coding: utf-8

from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression, Lasso, Ridge, SGDRegressor
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.svm import LinearSVR
from sklearn.tree import DecisionTreeRegressor, ExtraTreeRegressor

from settings import delta_transit, delta_preparation, delta_total
from utils import SessionRegistry, load_ytrain, choose_variable_to_predict

best_param = dict()

# Number of finished trials: 67
# Best trial:
#   Value: 0.32264396951997193
#   Params:
best_param[delta_transit] = {
    'n_estimators': 136,
    'min_samples_split': 0.0008081135809927323,
    'min_samples_leaf': 0.0029491417870389537,
    'max_features': 0.9994006884115223,
    'bootstrap': True
}

# Number of finished trials: 100
# Best trial:
#   Value: 0.034536511897253555.
#   Params:
best_param[delta_preparation] = {
    'n_estimators': 200,
    'min_samples_split': 0.004393930004791015,
    'min_samples_leaf': 0.002937356889392738,
    'max_features': 0.5041922826603705,
    'bootstrap': False
}

# Number of finished trials: 100
# Best trial:
#   Value: 0.3213217780434038
#   Params:
best_param[delta_total] = {
    'n_estimators': 66,
    'min_samples_split': 0.0007506199722069895,
    'min_samples_leaf': 0.0023805314257941755,
    'max_features': 0.9014672395948757,
    'bootstrap': False
}

if __name__ == "__main__":
    """
    Script to train the model for the transit time

     ⋅ Load selected dataset pairs
     ⋅ Train model
     ⋅ Save Session

    """
    to_predict = choose_variable_to_predict()

    registry = SessionRegistry()

    datasets_pair_folder, X, _ = registry.select_datasets_pair()
    y = load_ytrain()

    y = y.loc[X.index]

    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, random_state=1337, shuffle=True)

    # Dropping for now
    X_train = X_train.dropna()
    y_train = y_train.loc[X_train.index]

    X_val = X_val.dropna()
    y_val = y_val.loc[X_val.index]

    y_train = y_train[to_predict]
    y_val = y_val[to_predict]

    # Training

    models = [
        LinearRegression,
        ExtraTreeRegressor,
        RandomForestRegressor
    ]

    model_class = RandomForestRegressor

    print(f"Fitting {model_class.__name__} on {to_predict}")

    model = model_class(**best_param[to_predict])
    model.fit(X_train, y_train)

    y_pred = model.predict(X_val)

    r2 = r2_score(y_val, y_pred)
    mse = mean_squared_error(y_val, y_pred)
    print(f"R2 : {r2}")
    print(f"MSE: {mse}")

    scores = {"r2": r2, "mse": mse}

    registry.save_training_session(model,
                                   datasets_pair_folder,
                                   predicted_variable=to_predict,
                                   training_scores=scores)
