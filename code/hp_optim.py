#!/usr/bin/env python
# coding: utf-8

import lightgbm as lgb
import optuna
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.model_selection import train_test_split, ShuffleSplit, cross_val_score

from utils import SessionRegistry, load_ytrain, choose_variable_to_predict


def tune_lightgbm(trial):
    """
    Objective function to tune LightGBM

    :param trial:
    :return:
    """

    param = {
        'verbosity': -1,
        # max_depth : int, optional (default=-1)
        #     Maximum tree depth for base learners, <=0 means no limit.
        "n_estimators": trial.suggest_int("n_estimators", 10, 1000),
        # n_estimators : int, optional (default=100)
        #     Number of boosted trees to fit.

        # subsample_for_bin : int, optional (default=200000)
        #     Number of samples for constructing bins.

        # min_split_gain : float, optional (default=0.)
        #     Minimum loss reduction required to make a further partition on a leaf node of the tree.

        'boosting_type': 'gbdt',
        # boosting_type : string, optional (default='gbdt')
        #         'gbdt', traditional Gradient Boosting Decision Tree.
        #         'dart', Dropouts meet Multiple Additive Regression Trees.
        #         'goss', Gradient-based One-Side Sampling.
        #         'rf', Random Forest.

        'lambda_l1': trial.suggest_loguniform('lambda_l1', 1e-8, 10.0),
        # reg_alpha : float, optional (default=0.)
        #     L1 regularization term on weights.

        'lambda_l2': trial.suggest_loguniform('lambda_l2', 1e-8, 10.0),
        # reg_lambda : float, optional (default=0.)
        #     L2 regularization term on weights.

        'num_leaves': trial.suggest_int('num_leaves', 2, 256),
        # num_leaves : int, optional (default=31)
        #     Maximum tree leaves for base learners.

        'feature_fraction': trial.suggest_uniform('feature_fraction', 0.4, 1.0),
        'bagging_fraction': trial.suggest_uniform('bagging_fraction', 0.4, 1.0),
        'bagging_freq': trial.suggest_int('bagging_freq', 1, 7),
        'min_child_samples': trial.suggest_int('min_child_samples', 2, 100),
        # min_child_samples : int, optional (default=20)
        #     Minimum number of data needed in a child (leaf).
        "learning_rate": trial.suggest_loguniform("learning_rate", 1e-3, 1),
        "min_child_weight": trial.suggest_loguniform(
            "min_child_weight", 1e-5, 1e-1
        ),
    }

    gbm = lgb.LGBMRegressor(**param)

    cv = ShuffleSplit(n_splits=3, test_size=0.33)
    scores = cross_val_score(gbm, X, y, cv=cv, scoring='r2')

    r2 = scores.mean()
    print(f"Mean of R2s : {r2}")

    return r2


def tune_randomforest(trial: optuna.trial.Trial):
    """
    Objective function to tune RandomForrestRegressor

    :param trial:
    :return:
    """

    # See doc for description:
    # https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html
    param = {
        'n_estimators': trial.suggest_int('n_estimators', 20, 200),
        'min_samples_split': trial.suggest_uniform('min_samples_split', 0., 1.),
        'min_samples_leaf': trial.suggest_uniform('min_samples_leaf', 0., 0.5),
        'max_features': trial.suggest_uniform('max_features', 0., 1.),
        'bootstrap': trial.suggest_categorical('bootstrap', [True, False]),
        'criterion': "mse",
        "n_jobs": 2,
        "random_state": 1337
    }

    rf = RandomForestRegressor(**param)

    cv = ShuffleSplit(n_splits=3, test_size=0.2, random_state=0)
    scores = cross_val_score(rf, X, y, cv=cv, scoring='r2')

    r2 = scores.mean()
    print(f"Mean of R2s : {r2}")

    return r2


if __name__ == '__main__':
    to_predict = choose_variable_to_predict()

    registry = SessionRegistry()

    objectives = [tune_lightgbm, tune_randomforest]
    choice = -1

    while not(0 <= choice < len(objectives)):
        for i, obj in enumerate(objectives):
            print(i, obj)
        choice = int(input("Choice of the objective:"))

    objective_function = objectives[choice]

    # Selecting dataset
    datasets_pair_folder, X, _ = registry.select_datasets_pair()
    X = X.dropna()

    y = load_ytrain()
    y = y.loc[X.index]
    y = y[to_predict]

    # Setting the study with optuna
    study = optuna.create_study(direction='maximize')

    study.optimize(objective_function, n_trials=1000)

    print(f"Objective: {objective_function}")
    print(f"Variable: {to_predict}")
    print(f"Dataset: {datasets_pair_folder}")
    print()

    print('Number of finished trials: {}'.format(len(study.trials)))

    print(f'Best trial:')
    trial = study.best_trial

    print('  Value: {}'.format(trial.value))

    print('  Params: ')
    for key, value in trial.params.items():
        print('    {}: {}'.format(key, value))
