#!/usr/bin/env python
# coding: utf-8

from keras.layers.core import Dense
from keras.models import Sequential
from keras.optimizers import Adam
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.model_selection import train_test_split

from utils import SessionRegistry, load_ytrain, r2_keras, choose_variable_to_predict


def neural_net(input_dim):
    """
    Return a simple feed forward MLP


    :param input_dim:
    :return:
    """
    model = Sequential(name="Simple FeedForward")
    model.add(Dense(40, input_dim=input_dim, activation="relu"))
    model.add(Dense(40, activation="relu"))
    model.add(Dense(20, activation="relu"))
    model.add(Dense(20, activation="relu"))
    model.add(Dense(20, activation="relu"))
    model.add(Dense(8, activation="relu"))
    model.add(Dense(8, activation="relu"))
    model.add(Dense(4, activation="relu"))
    model.add(Dense(4, activation="relu"))
    model.add(Dense(4, activation="relu"))
    model.add(Dense(1, activation="linear"))

    return model


if __name__ == "__main__":
    """
    Script to train the model for the preparation time

     ⋅ Load selected dataset pairs
     ⋅ Train model
     ⋅ Save Session

    """

    to_predict = choose_variable_to_predict()

    registry = SessionRegistry()

    datasets_pair_folder, X, _ = registry.select_datasets_pair()
    y = load_ytrain()

    y = y.loc[X.index]

    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, random_state=1337, shuffle=True)

    # Dropping for now
    X_train = X_train.dropna()
    y_train = y_train.loc[X_train.index]

    X_val = X_val.dropna()
    y_val = y_val.loc[X_val.index]

    y_train = y_train[to_predict]
    y_val = y_val[to_predict]

    # Training

    # To numpy array for Keras
    X_train = X_train.__array__()
    y_train = y_train.__array__()
    X_val = X_val.__array__()
    y_val = y_val.__array__()

    # Really simple neural net
    model = neural_net(input_dim=X_train.shape[1])

    opt = Adam()
    model.compile(loss="mean_squared_error", optimizer=opt, metrics=[r2_keras])

    print(f"Fitting {model.name}")

    model.fit(X_train, y_train,
              validation_data=(X_val, y_val),
              epochs=15,
              batch_size=16)

    y_pred = model.predict(X_val)

    r2 = r2_score(y_val, y_pred)
    mse = mean_squared_error(y_val, y_pred)
    print(f"R2 : {r2}")
    print(f"MSE: {mse}")

    scores = {"r2": r2, "mse": mse}

    registry.save_training_session(model,
                                   datasets_pair_folder,
                                   predicted_variable=to_predict,
                                   training_scores=scores)