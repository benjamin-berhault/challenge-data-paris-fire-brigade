import numpy as np

from sklearn.ensemble import RandomForestRegressor
from sklearn.inspection import plot_partial_dependence

from lightgbm import plot_importance, plot_metric, LGBMRegressor, \
    create_tree_digraph
from sklearn.linear_model import LinearRegression

from utils import SessionRegistry

import matplotlib.pyplot as plt

if __name__ == "__main__":
    """
    Script to interpreter models
    
    For RandomForests and LightGBM boosters:
     ⋅ we get their features importance, that is a score indicating
     for each feature, the number of times it has been used
    
    For Linear Regression:
     ⋅ we plot the coefficients associated to features
     
    For all models:
     ⋅ we plot partial dependence for each feature, i.e. average response
     for the model for each feature
    
    
    """
    registry = SessionRegistry()

    model, X_train, X_test = registry.select_session()

    X_train = X_train.dropna()

    features_names = np.array(X_train.keys())

    if isinstance(model, LGBMRegressor):
        plot_importance(model)
        plot_metric(model)
        graph = create_tree_digraph(model)
        graph.render()

    if isinstance(model, RandomForestRegressor):
        importances = model.feature_importances_
        std = np.std([tree.feature_importances_ for tree in model.estimators_],
                     axis=0)
        indices = np.argsort(importances)[::-1]

        print("Feature ranking:")

        for f in range(X_train.shape[1]):
            print(f"{f + 1}. {features_names[indices[f]]} : {importances[indices[f]]}")

        plt.figure(figsize=(20, 10))
        plt.title("Feature importances")
        y_pos = range(X_train.shape[1])
        ax = plt.gca()
        ax.barh(y_pos,
                importances[indices],
                xerr=std[indices], align="center")
        ax.set_yticks(y_pos)
        ax.set_yticklabels(features_names[indices])
        ax.invert_yaxis()  # labels read top-to-bottom
        ax.set_xlabel('Feature importances')
        plt.show()

    if isinstance(model, LinearRegression):
        importances = model.coef_
        indices = np.argsort(importances)[::-1]

        # Print the feature ranking
        print("Features coefficients:")

        for f in range(X_train.shape[1]):
            print(f"{f + 1}. {features_names[indices[f]]} : {importances[indices[f]]}")

        plt.figure(figsize=(20, 10))
        plt.title("Coefficients")
        y_pos = range(X_train.shape[1])
        ax = plt.gca()
        ax.barh(y_pos,
                importances[indices], align="center")
        ax.set_yticks(y_pos)
        ax.set_yticklabels(features_names[indices])
        ax.invert_yaxis()  # labels read top-to-bottom
        ax.set_xlabel('Coefficients')
        plt.show()

    # Experimental
    # Plotting partial dependences
    # https://scikit-learn.org/stable/modules/partial_dependence.html
    if False:
        n_features = len(features_names)
        n_features_to_plot = 8
        n_plots = int(np.ceil(n_features / n_features_to_plot))

        for i in range(n_plots):
            start = i*n_features_to_plot
            end = min((i + 1) * n_features_to_plot, n_features)
            features_to_plot = features_names[start:end]

            print(f'Computing partial dependence plots... {i+1} / {n_plots}')
            plot_partial_dependence(model, X_train[features_to_plot],
                                    features=features_to_plot,
                                    feature_names=features_to_plot,
                                    n_jobs=4,
                                    grid_resolution=20)

            fig = plt.gcf()
            fig.suptitle(f'Partial dependence {i+1} / {n_plots}')
            fig.subplots_adjust(wspace=0.4, hspace=0.3)
        plt.show()
