#!/usr/bin/env python
# coding: utf-8

import os
from os.path import join as opj
from pathlib import Path

import joblib
import namegenerator
import pandas as pd
import json
import numpy as np
from keras import backend as K
from numba import njit

from datetime import datetime

from settings import SESSIONS_FOLDER, TRANSFORMED_DATA, X_TRAIN, X_TEST, INDEX_COLUMN, Y_TRAIN, n_test, CACHE, n_train, \
    TO_PREDICT, SUBMISSION_FOLDER


def now_string():
    """
    Return a string of the current datetime.

    :return:
    """
    return datetime.now().strftime("%Y_%m_%d_%H_%M_%S")


def load_x(path: Path) -> pd.DataFrame:
    """
    Load a data set as a correctly indexed pandas DataFrame.

    :param path: path of the csv
    :return:
    """
    # Ugly
    if path == X_TRAIN or path == X_TEST:
        df = pd.read_csv(path, index_col=INDEX_COLUMN, parse_dates=["selection time"])
    else:
        df = pd.read_csv(path, index_col=INDEX_COLUMN)

    return df


def load_ytrain() -> pd.DataFrame:
    """
    Load the training label set as a correctly indexed pandas DataFrame.

    :return:
    """
    return pd.read_csv(Y_TRAIN, index_col=INDEX_COLUMN)


@CACHE.cache
def train_index():
    return load_x(X_TRAIN).index


@CACHE.cache
def test_index():
    return load_x(X_TEST).index


class SessionRegistry:
    """
    An interface to interact with dataset and session:

        ⋅

    """

    def __init__(self):

        self._stub_joblib = 'model.joblib'
        self._stub_keras = 'model.h5'
        self._stub_training_score = 'training_score.json'
        self._stub_h_parameters = 'hyper_parameters.json'
        self._stub_xtrain = "x_train.csv"
        self._stub_xtest = "x_test.csv"
        self._stub_dataset_name = "datasets_pair.txt"
        self._stub_predicted_var = "predicted_variable.txt"

    def save_training_session(self, model,
                              datasets_pair_name,
                              predicted_variable, training_scores=None, h_parameters=None):
        """
        Save information about a training session

        :param training_scores:
        :param h_parameters: dict
        :param model: dict
        :return:
        """
        name = f"{now_string()}_{model.__class__.__name__}_{namegenerator.gen()}"
        folder = SESSIONS_FOLDER / name
        os.makedirs(folder)

        from keras import Sequential
        if isinstance(model, Sequential):
            model.save(folder / self._stub_keras)
        else:
            joblib.dump(model, folder / self._stub_joblib)

        with open(folder / self._stub_dataset_name, "w") as fp:
            fp.write(str(datasets_pair_name))

        with open(folder / self._stub_predicted_var, "w") as fp:
            fp.write(predicted_variable)

        if training_scores is not None:
            with open(folder / self._stub_training_score, "w") as fp:
                json.dump(training_scores, fp)
        if h_parameters is not None:
            with open(folder / self._stub_h_parameters, "w") as fp:
                json.dump(h_parameters, fp)

        print(f"Session saved in {os.path.basename(folder)}")

    def select_session(self):
        """
        Load the model and associated pair of datasets from disk.

        :return:
        """

        sessions = sorted(os.listdir(SESSIONS_FOLDER))
        choice = -1
        while not (0 <= choice < len(sessions)):
            for i, session in enumerate(sessions):
                print(i, os.path.basename(session))
            choice = int(input("Choice of the session:"))

        folder = SESSIONS_FOLDER / sessions[choice]

        model = None

        if os.path.exists(folder / self._stub_joblib):
            model = joblib.load(SESSIONS_FOLDER / sessions[choice] / self._stub_joblib)
        elif os.path.exists(folder / self._stub_keras):
            import keras
            model = keras.models.load_model(folder / self._stub_keras, custom_objects={"r2_keras": r2_keras})
        else:
            raise RuntimeError(f"No model found for {os.path.basename(sessions[choice])}")

        if os.path.exists(folder / self._stub_dataset_name):
            with open(folder / self._stub_dataset_name, "r") as fp:
                datasets_pair_folder = Path(fp.read().strip())
        else:
            raise RuntimeError(f"Data set info is not there")

        if not (os.path.exists(datasets_pair_folder)):
            raise RuntimeError(f"{datasets_pair_folder} does not there")

        X_train = load_x(datasets_pair_folder / self._stub_xtrain)
        X_test = load_x(datasets_pair_folder / self._stub_xtest)

        return model, X_train, X_test

    def save_datasets_pair(self, x_train: pd.DataFrame, x_test: pd.DataFrame, datasets_pair_name=None):
        """
        Set a pair of preprocessed datasets for later use.

        Ask for a name of the dataset.
        """

        # Some book keeping pre-dump
        assert (x_test.keys() == x_train.keys()).all()
        assert (n_test == x_test.shape[0])
        assert (n_train == x_train.shape[0])

        x_train = x_train.loc[train_index()]
        x_test = x_test.loc[test_index()]

        if datasets_pair_name is None:
            already_present = True
            while already_present:
                datasets_pair_name = input("Give a name to the pair of datasets: ")
                already_present = os.path.exists(TRANSFORMED_DATA / datasets_pair_name)
                if already_present:
                    print(f"{datasets_pair_name} is already present")

        folder = TRANSFORMED_DATA / datasets_pair_name
        os.makedirs(folder, exist_ok=True)
        x_train.to_csv(folder / self._stub_xtrain)
        x_test.to_csv(folder / self._stub_xtest)

        print(f"Datasets saved in {folder}")

    def select_datasets_pair(self):
        """
        Select a pair of preprocessed datasets to load.

        :return:
        """
        pairs = sorted(os.listdir(TRANSFORMED_DATA))
        choice = -1
        while not (0 <= choice < len(pairs)):
            print("Dataset pairs available")
            for i, session in enumerate(pairs):
                print(i, os.path.basename(session))
            choice = int(input("Choice of the pair:"))

        datasets_pair_folder = TRANSFORMED_DATA / pairs[choice]

        X_train = load_x(datasets_pair_folder / self._stub_xtrain)
        X_test = load_x(datasets_pair_folder / self._stub_xtest)

        return datasets_pair_folder, X_train, X_test


def _save_submission(emergency_vehicule_selection,
                     delta_selection_departure,
                     delta_departure_presentation,
                     delta_selection_presentation, suffix):
    """


    :param emergency_vehicule_selection:
    :param delta_selection_departure:
    :param delta_departure_presentation:
    :param delta_selection_presentation:
    :param suffix:
    :return:
    """
    submission = pd.concat([emergency_vehicule_selection,
                            delta_selection_departure,
                            delta_departure_presentation,
                            delta_selection_presentation],
                           axis=1)

    submission.columns = [INDEX_COLUMN, *TO_PREDICT]

    submission.set_index(INDEX_COLUMN, inplace=True)

    if suffix is None:
        suffix = now_string()

    csv_file = opj(SUBMISSION_FOLDER, f'submission_{suffix}.csv')

    submission.to_csv(csv_file, sep=",")
    print(f"Submission created at {csv_file}")


def create_submission(emergency_vehicule_selection,
                      delta_selection_departure,
                      delta_departure_presentation,
                      suffix=None):
    """
    Create a submission file for

    :param emergency_vehicule_selection: np array of ids
    :param delta_selection_departure: np array of floats
    :param delta_departure_presentation: np array of floats
    :param suffix:
    :return:
    """

    delta_selection_presentation = delta_selection_departure + delta_departure_presentation

    _save_submission(emergency_vehicule_selection,
                     delta_selection_departure,
                     delta_departure_presentation,
                     delta_selection_presentation, suffix)


def create_submission_whole(emergency_vehicule_selection,
                            delta_selection_departure,
                            delta_departure_presentation,
                            delta_selection_presentation,
                            suffix=None):
    """
    Create a submission file

    :param emergency_vehicule_selection: np array of ids
    :param delta_selection_departure: np array of floats
    :param delta_departure_presentation: np array of floats
    :param suffix:
    :return:
    """
    _save_submission(emergency_vehicule_selection,
                     delta_selection_departure,
                     delta_departure_presentation,
                     delta_selection_presentation, suffix)


def create_submission_delta_selection_presentation(emergency_vehicule_selection,
                                                   delta_selection_presentation,
                                                   suffix=None):
    """
    Create a submission file

    :param emergency_vehicule_selection: np array of ids
    :param delta_selection_presentation: np array of floats
    :param suffix:
    :return:
    """

    delta_selection_departure = delta_selection_presentation / 2
    delta_departure_presentation = delta_selection_presentation / 2

    _save_submission(emergency_vehicule_selection,
                     delta_selection_departure,
                     delta_departure_presentation,
                     delta_selection_presentation, suffix)


def r2_keras(y_true, y_pred):
    """
    Custom R2-score metrics for keras backend

    See: https://www.kaggle.com/c/mercedes-benz-greener-manufacturing/discussion/34019

    :param y_true:
    :param y_pred:
    :return:
    """
    SS_res = K.sum(K.square(y_true - y_pred))
    SS_tot = K.sum(K.square(y_true - K.mean(y_true)))
    return 1 - SS_res / (SS_tot + K.epsilon())


def choose_variable_to_predict():
    """
    Simple util function to select the variable to predict.

    :return:
    """

    choice = -1
    while not (0 <= choice < len(TO_PREDICT)):
        for i, var in enumerate(TO_PREDICT):
            print(i, var)
        choice = int(input("Choice of the variable to predict:"))

    return TO_PREDICT[choice]


def convert_time(x):
    d, t = x[0], x[1]
    if t < 30000:
        return 10**6 * d + 30000
    elif t < 60000:
        return 10**6 * d + 60000
    elif t < 90000:
        return 10**6 * d + 90000
    elif t < 120000:
        return 10**6 * d + 120000
    elif t < 150000:
        return 10**6 * d + 150000
    elif t < 180000:
        return 10**6 * d + 180000
    elif t < 210000:
        return 10**6 * d + 210000
    else:
        return 10**6 * (d+1)


def replace_missing_databy_median(s):
    """
    Replace missing data in a series with
    the median of non missing values

    :param s:
    :return:
    """
    series = pd.to_numeric(s.replace('mq', np.NaN))
    return series.replace(np.NaN, np.median(series[series != np.NaN]))
