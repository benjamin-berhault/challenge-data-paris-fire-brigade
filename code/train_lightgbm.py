#!/usr/bin/env python
# coding: utf-8
import lightgbm as lgb
import numpy as np
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.model_selection import train_test_split

from settings import delta_preparation, delta_transit
from utils import SessionRegistry, load_ytrain, choose_variable_to_predict


def rmsle(y_true, y_pred):
    """
    Root Mean Squared Logarithmic Error (RMSLE)

    :param y_true:
    :param y_pred:
    :return:
    """
    is_higher_better = False

    return 'RMSLE', np.sqrt(np.mean(np.power(np.log1p(y_pred) - np.log1p(y_true), 2))), is_higher_better


def rae(y_true, y_pred):
    """
    Relative Absolute Error (RAE)

    :param y_true:
    :param y_pred:
    :return:
    """
    is_higher_better = False

    return 'RAE', np.sum(np.abs(y_pred - y_true)) / np.sum(np.abs(np.mean(y_true) - y_true)), is_higher_better


def r2(y_true, y_pred):
    """

    :param y_true:
    :param y_pred:
    :return:
    """
    is_higher_better = True

    return 'R2', r2_score(y_true, y_pred), is_higher_better


def mse(y_true, y_pred):
    """

    :param y_true:
    :param y_pred:
    :return:
    """
    is_higher_better = False

    return 'MSE', mean_squared_error(y_true, y_pred), is_higher_better


best_param = dict()

# R2: 0.20817470864426732
best_param[delta_preparation] = {
    'n_estimators': 945,
    'lambda_l1': 0.4690195480615238,
    'lambda_l2': 2.0041751335751564e-08,
    'num_leaves': 243,
    'feature_fraction': 0.5324569983840927,
    'bagging_fraction': 0.7751678871272152,
    'bagging_freq': 4,
    'min_child_samples': 69,
    'learning_rate': 0.012557469244190506,
    'min_child_weight': 0.0015376047104377576
}

# R2: 0.4063488289838511
best_param[delta_transit] = {
    'n_estimators': 265,
    'lambda_l1': 0.0004115530758041674,
    'lambda_l2': 0.00027152281406684877,
    'num_leaves': 166,
    'feature_fraction': 0.8589537129434159,
    'bagging_fraction': 0.5820529029446255,
    'bagging_freq': 5,
    'min_child_samples': 65,
    'learning_rate': 0.010538052781074092,
    'min_child_weight': 3.293526958956054e-05
}


if __name__ == "__main__":
    """
    Script to train the model for the transit time

     ⋅ Load selected dataset pairs
     ⋅ Train model
     ⋅ Save Session

    """

    to_predict = choose_variable_to_predict()

    registry = SessionRegistry()

    datasets_pair_folder, X, _ = registry.select_datasets_pair()
    y = load_ytrain()

    y = y.loc[X.index]

    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, random_state=1337, shuffle=True)

    X_train = X_train.dropna()
    y_train = y_train.loc[X_train.index]

    X_val = X_val.dropna()
    y_val = y_val.loc[X_val.index]

    y_train = y_train[to_predict]
    y_val = y_val[to_predict]

    print('Starting training...')
    gbm = lgb.LGBMRegressor(**best_param[to_predict])

    print('Starting training with multiple custom eval functions...')
    # train
    gbm.fit(X_train, y_train,
            eval_set=[(X_val, y_val)],
            eval_metric=lambda y_true, y_pred: [rmsle(y_true, y_pred),
                                                rae(y_true, y_pred),
                                                mse(y_true, y_pred),
                                                r2(y_true, y_pred)
                                                ])

    print('Feature importances:', list(gbm.feature_importances_))

    print('Starting predicting...')

    y_pred = gbm.predict(X_val, num_iteration=gbm.best_iteration_)

    scores = {
        'RMSLE:': rmsle(y_val, y_pred)[1],
        'RAE:': rae(y_val, y_pred)[1],
        'MSE:': mse(y_val, y_pred)[1],
        'R2:': r2(y_val, y_pred)[1]
    }

    print("Score:")
    for k, v in scores.items():
        print(k, v)

    registry.save_training_session(gbm,
                                   datasets_pair_folder,
                                   predicted_variable=to_predict,
                                   training_scores=scores)
