#!/usr/bin/env python
# coding: utf-8

import pandas as pd

from utils import SessionRegistry, create_submission

if __name__ == "__main__":
    """
    Script for submission

      ⋅ Let the user select the model for the preparation time
      ⋅ Let the user select the model for the transit time
      ⋅ Predict preparation time on test data
      ⋅ Predict transit time on test data
      ⋅ Create a submission with predictions

    :return:
    """
    registry = SessionRegistry()

    print("Select model for delta preparation")

    preparation_model, _, X_test_prep = registry.select_session()
    X_test_prep = X_test_prep.fillna(X_test_prep.median())

    print("Select model for delta transit")

    transit_model, _, X_test_trans = registry.select_session()
    X_test_trans = X_test_trans.fillna(X_test_trans.median())

    delta_prep = pd.DataFrame(preparation_model.predict(X_test_prep))
    delta_trans = pd.DataFrame(transit_model.predict(X_test_trans))

    index_test = pd.DataFrame(X_test_trans.index)
    create_submission(emergency_vehicule_selection=index_test,
                      delta_selection_departure=delta_prep,
                      delta_departure_presentation=delta_trans)
