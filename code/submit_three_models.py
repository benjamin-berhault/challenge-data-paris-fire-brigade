#!/usr/bin/env python
# coding: utf-8

import pandas as pd

from utils import SessionRegistry, create_submission_whole

if __name__ == "__main__":
    """
    Script for submission

      ⋅ Let the user select the model for the preparation time
      ⋅ Let the user select the model for the transit time
      ⋅ Predict preparation time on test data
      ⋅ Predict transit time on test data
      ⋅ Create a submission with predictions

    :return:
    """
    registry = SessionRegistry()

    print("Select model for delta preparation")

    preparation_model, _, X_test_prep = registry.select_session()
    X_test_prep = X_test_prep.fillna(X_test_prep.median())

    print("Select model for delta transit")

    transit_model, _, X_test_trans = registry.select_session()
    X_test_trans = X_test_trans.fillna(X_test_trans.median())

    print("Select model for delta total")

    global_model, _, X_glob = registry.select_session()
    X_glob = X_glob.fillna(X_glob.median())

    delta_prep = pd.DataFrame(preparation_model.predict(X_test_prep))
    delta_trans = pd.DataFrame(transit_model.predict(X_test_trans))
    delta_glob = pd.DataFrame(global_model.predict(X_glob))

    index_test = pd.DataFrame(X_test_trans.index)
    create_submission_whole(emergency_vehicule_selection=index_test,
                            delta_selection_departure=delta_prep,
                            delta_departure_presentation=delta_trans,
                            delta_selection_presentation=delta_glob)
