import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from datetime import date
from scipy.stats.stats import pearsonr   

# on prend tous les fichiers météo
l_meteo = []
for month in range(1, 13):
	l_meteo.append(pd.read_csv('../meteo/synop.2018%s.csv' % (str(month).zfill(2)), sep=';'))
data_meteo = pd.concat(l_meteo)

# on garde que les mesures à Orly (la plus proche)
data_meteo = data_meteo[data_meteo['numer_sta'] == 7149]
data_meteo.index = data_meteo['date']


def convert_time(x):
	d, t = x[0], x[1]
	if t < 30000:
		return 10**6 * d + 30000
	elif t < 60000:
		return 10**6 * d + 60000
	elif t < 90000:
		return 10**6 * d + 90000
	elif t < 120000:
		return 10**6 * d + 120000
	elif t < 150000:
		return 10**6 * d + 150000
	elif t < 180000:
		return 10**6 * d + 180000
	elif t < 210000:
		return 10**6 * d + 210000
	else:
		return 10**6 * (d+1)

X = pd.read_csv('../data/train/x_train.csv', sep=',')
Y = pd.read_csv('../data/train/y_train.csv', sep=',')
X = X.drop(columns=["emergency vehicle selection", "mydatavention",
"alert reason category", 
"alert reason",
"mydatavention on public roads",
"floor",
"location of the event",
"longitude mydatavention",
"latitude mydatavention",
"emergency vehicle",
"emergency vehicle type",
"rescue center",
"status preceding selection",
"delta status preceding selection-selection",
"departed from its rescue center",
"longitude before departure",
"latitude before departure",
"delta position gps previous departure-departure",
"GPS tracks departure-presentation",
"GPS tracks datetime departure-presentation",
"OSRM response",
"OSRM estimated distance",
"OSRM estimated duration"])

X['date'] = X[['date key sélection', 'time key sélection']].apply(convert_time, axis=1).astype('int64')
X_meteo = X.join(data_meteo, on='date', lsuffix='_caller', rsuffix='_other', how='left')

#print(X_meteo.info())
def transform(s):
	mydata = pd.to_numeric(s.replace('mq', np.NaN))
	return mydata.replace(np.NaN, np.mean(mydata[mydata != np.NaN]))

features = ['pmer', 'tend', 'dd', 'ff', 't', 'u', 'vv', 'n', 'pres', 'raf10', 'rafper', 'ht_neige', 'ssfrai', 'rr1', 'rr3', 'rr6', 'rr12', 'rr24']

# quelles colonnes choisir ?
# https://support.minitab.com/en-us/minitab-express/1/help-and-how-to/modeling-statistics/regression/how-to/correlation/mydatapret-the-results/
# choix assez clair en pratique, quelques colonnes sortent du lot pour la selection

print("before departure")
for f in features:
	val = pearsonr(transform(X_meteo[f]), Y['delta selection-departure'])
	if val[1] < 1e-10 and np.abs(val[0]) > 0.04:
		print(f, val)

# curieusement moins net pour le trajet
print()
print("after departure")
for f in features:
	val = pearsonr(Y['delta departure-presentation'], transform(X_meteo[f]))
	if val[1] < 1e-10 and np.abs(val[0]) > 0.01:
		print(f, val)

	
"""
Conclusion ;
on a les résultats suivants: 
before departure
ff : Vitesse du vent moyen 10 mn (-0.042954300319122965, 4.355898598079199e-90)
t : Température (-0.04828391857668064, 2.4000509047496903e-113)
u : Humidité (0.08560839233025483, 0.0)
vv : Visibilité horizontale (-0.04244824644535774, 5.085805826759263e-88)
raf10 : Rafales sur les 10 dernières minutes (-0.04690419958071442, 4.592880784075787e-107)
rafper : Rafales sur une période (-0.05495132824992889, 2.840453353805092e-146)

after departure
ff (0.017824536369202552, 6.916049117762307e-17)
u (0.015560447947852562, 3.148448238819271e-13)
vv (-0.021685714395905643, 3.074735435612386e-24)
n (0.01517617911748297, 1.1782721376325757e-12)
raf10 (0.01948866599281225, 6.976013987261332e-20)
rafper (0.018822544423120247, 1.1864952109144776e-18)
ht_neige : Hauteur totale de la couche de neige, glace, autre (0.021049876874504183, 6.241188551627223e-23)
ssfrai :Hauteur de la neige fraîche (0.023639219555674863, 1.7049655061605335e-28)
rr6 : Précipitations dans les N dernières heures(0.016986125402655476, 1.781022156986153e-15)
rr12 (0.019092216823624813, 3.8114047642575274e-19)
rr24 (0.018152320301429745, 1.8634236146962315e-17)

On voit que les infos sont redondantes d'après leur description, on prend donc la meilleur dans chaque domaine
rafper
t (pour la préparation)
vv
u
ssfrai
rr12

Le choix de prendre des mesures sur des temps longs est judicieux car peu de données (4 par jour)
le choix de la neige fraiche est mieux que neige totale : seule la neige fraiche peut gêner à Paris, 
elle disparait en une journée, fonc l'accumulation vue à Orly n'est pas significatif (plus froid, c'est presque la province !)
"""