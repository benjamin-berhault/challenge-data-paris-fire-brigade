#!/usr/bin/env python
# coding: utf-8

"""
Generate the OSRM Dataset used to get itinary for each emergency vehicule
selection based on the type of the vehicle.

See preprocessing.add_traffic_estimation.

"""

import time

import polyline
from joblib import Parallel, delayed

from interfaces import *
from preprocessing import add_vehicle_info
from settings import n_test, n_train, X_TRAIN, X_TEST, OSRM_ESTIMATES_CSV
from utils import load_x


def connector(lon_start, lat_start, lon_end, lat_end,
              heavy_vehicle, very_heavy_vehicle):
    """
    Connect to server and get new OSMR info.

    Cumbersome exception handling as of now.

    :param lon_start:
    :param lat_start:
    :param lon_end:
    :param lat_end:
    :return:
    """

    if heavy_vehicle == 1.0 or very_heavy_vehicle == 1.0:
        profile = "heavy"
    else:
        profile = 'car'

    response = None
    time.sleep(0.01)
    try:
        response = get_itinary(lon_start, lat_start, lon_end, lat_end, profile)
        itinary = response["routes"][0]
        distance = itinary["distance"]
        duration = itinary["duration"]
        polyline_length = len(polyline.decode(itinary["geometry"]))
        return [lon_start, lat_start, lon_end, lat_end, distance, duration, polyline_length]
    except Exception as e:
        print(f"Exception for ({lon_start}, {lat_start}, {lon_end}, {lat_end}): ")
        print(e)
        # We put the response and the exception instead of the distance, the duration and polyline_length here
        res = [lon_start, lat_start, lon_end, lat_end, None, response, e]
        failed_request.append([lon_start, lat_start, lon_end, lat_end, None, response, e])

        return res


if __name__ == "__main__":
    failed_request = []
    logging.info(" → STEP: Loading data sets")

    X = pd.concat((load_x(X_TRAIN), load_x(X_TEST)), sort=False)

    X = add_vehicle_info(X)

    assert X.shape[0] == n_test + n_train

    # Keeping useful values only
    X_reduced = X[["longitude before departure", "latitude before departure",
                   "longitude intervention", "latitude intervention",
                   'heavy vehicle', 'very heavy vehicle']]

    index = X_reduced.index

    X_np_array = X_reduced.values

    res_batches = []
    nb_batches = 1000
    nb_request_per_batch = len(X_reduced) // nb_batches

    # We process batches sequentially with some pause
    # not to spam to server
    for i in range(nb_batches):
        left = i * nb_request_per_batch
        right = (i + 1) * nb_request_per_batch
        print(f"Parallel from {left} to {right}")
        res_batches.append(Parallel(n_jobs=4, verbose=1)(delayed(connector)(*row)
                                                         for row in X_np_array[left:right, :]))
        time.sleep(10)

    res_batches.append(Parallel(n_jobs=4, verbose=1)(delayed(connector)(*row)
                                                     for row in X_np_array[right:, :]))

    # Flattening all
    res = [elem for sublist in res_batches for elem in sublist]

    failed = []
    for i in range(len(res)):
        element = res[i]
        if element[4] is None:
            failed.append((i, element))

    pd_res = pd.DataFrame(res, index=index, columns=["longitude before departure", "latitude before departure",
                                                     "longitude intervention", "latitude intervention",
                                                     "distance", "duration", "nb_intersections"])
    final = pd_res.fillna(pd_res.mean())

    final.to_csv(OSRM_ESTIMATES_CSV)
