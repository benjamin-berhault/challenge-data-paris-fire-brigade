#!/usr/bin/env python
# coding: utf-8
import datetime
import json
import logging
import os
from abc import abstractmethod, ABC

import ipywidgets
import numpy as np

import pandas as pd
import requests
import utm
from IPython.lib.display import IFrame
from ipyleaflet import Map, TileLayer, GeoJSON
from sklearn.neighbors import KDTree

from sqlalchemy import (
    create_engine,
    Column,
    Integer,
    String,
    ForeignKey,
    Float,
    JSON
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from tqdm import tqdm

from settings import DB_URL_FILE, LOCAL_DATABASE, TRAFFIC_DATA, DATE_FORMAT_DATASET
from utils import SessionRegistry

logging.basicConfig(level=logging.INFO)
pd.options.mode.chained_assignment = "raise"

Base = declarative_base()


class Node(Base):
    """
    Information related to a node in the graph of roads
    and traffic infrastructure.

    `iu_ac` uniquely identifies a node.

    `geo_json` specifies the shape of the node on the map
    (usually a `Line`).

    See: https://opendata.paris.fr/explore/dataset/referentiel-comptages-routiers/information/


    """
    __tablename__ = "node"

    iu_ac = Column(Integer, primary_key=True)
    # date_debut = Column(DateTime)
    # date_fin = Column(DateTime)
    libelle = Column(String)
    iu_nd_aval = Column(Integer, ForeignKey('node.iu_ac'))
    iu_nd_amont = Column(Integer, ForeignKey('node.iu_ac'))
    lon = Column(Float(precision=64), index=True)
    lat = Column(Float(precision=64), index=True)
    geo_json = Column(JSON)


class TrafficInfo(Base):
    """
    Information related to the traffic.

    This information is related to a `Node` via `iu_ac`

    k and q are metrics for the traffic.


    """
    __tablename__ = "traffic_info"

    objectid = Column(Integer, primary_key=True)
    iu_ac = Column(Integer, ForeignKey('node.iu_ac'), index=True)
    # t_1h = Column(DateTime)
    year = Column(Integer, index=True)
    month = Column(Integer, index=True)
    day = Column(Integer, index=True)
    hour = Column(Integer, index=True)
    q = Column(Float(precision=64))
    k = Column(Float(precision=64))
    etat_traffic = Column(Integer)
    etat_barre = Column(Integer)

    node = relationship("Node", back_populates="traffic_infos")


# Foreign Keys Management
Node.traffic_infos = relationship("TrafficInfo", back_populates="node")


def recreate_db(verbose=False):
    print("Resetting database")
    db_string = get_db_string()
    engine = create_engine(db_string, echo=verbose)
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)
    print("Database reset")


def get_db_string(force_local=False, database_url_file: str = DB_URL_FILE):
    """
    Returns the url of the database to use based on the configuration.

    If the URL is not present in the configuration, a local SQLite database
    will be used.

    :param force_local: If True, will return the database string of the local SQLite database.
    :param database_url_file:
    :return:
    """

    if not force_local and os.path.exists(database_url_file):
        file = open(database_url_file, "r")
        content = file.readline()
        file.close()
        parsed = content.replace('"', "").replace("\n", "")
        if len(parsed) > 0:
            print("Using remote database")
            return parsed

    db_string = "sqlite:///" + LOCAL_DATABASE
    print("Using local SQLite database : {}".format(db_string))
    parent_dir = os.path.dirname(database_url_file)
    os.makedirs(parent_dir, exist_ok=True)
    return db_string


def csv_to_sql():
    """
    Import data related to the traffic into
    a SQL data base speficied by `get_db_string()`
    """
    # Cleaning things
    recreate_db()

    db_string = get_db_string()
    engine = create_engine(db_string, echo=False)

    ti = CSVTrafficInterface()
    ti.node_to_sql_table(engine)

    ti.traffic_info_to_sql_table(engine)


class TrafficInterface(ABC):
    """
    A general interface for interfacing with data from the traffic.
    """

    def __init__(self):
        self._q_year = 2018

    @abstractmethod
    def get_traffic_data_of_date(self, date_string, running_traffic_only=False):
        raise NotImplementedError()

    @abstractmethod
    def get_estimated_traffic(self, q_lon, q_lat, date_time_string):
        raise NotImplementedError()


class CSVTrafficInterface(TrafficInterface):

    def __init__(self):
        super().__init__()

        self._traffic_relevant_columns = ['iu_ac', 't_1h', 'q', 'k', 'etat_trafic', 'etat_barre']

        # Dataset containing info about street etc. (see sessions.Node)
        self._df_node = self._load_reff()

        # Files of dataset containing info about traffic (see sessions.TrafficInfo)
        self._traffic_files = sorted([f for f in os.listdir(TRAFFIC_DATA) if "trafic_capteurs" in f])

    @staticmethod
    def _load_reff():
        """
        Extract reference road infrastructure from paris and insert it
        in given database table.

        see: https://opendata.paris.fr/explore/dataset/referentiel-comptages-routiers/information/

        :return:
        """
        df_node = pd.read_csv(TRAFFIC_DATA / "referentiel-comptages-routiers.csv", delimiter=";")

        columns_convertion = {
            'IU_AC': 'iu_ac',
            'DATE_DEBUT': 'date_debut',
            'DATE_FIN': 'date_fin',
            'LIBELLE': 'libelle',
            'IU_ND_AVAL': 'iu_nd_aval',
            'IU_ND_AMONT': 'iu_nd_amont',
            'geo_shape': 'geo_json',
        }
        df_node = df_node.rename(columns=columns_convertion)

        lonlat = pd.DataFrame(df_node.geo_point_2d.str.split(',', 1).tolist(), columns=['lat', 'lon'])

        df_node["lon"] = lonlat.lon.astype(float)
        df_node["lat"] = lonlat.lat.astype(float)

        df_node.iu_nd_aval = df_node.iu_nd_aval.astype(int)
        df_node.iu_nd_amont = df_node.iu_nd_amont.astype(int)

        col_table_db = [
            'iu_ac',
            'libelle',
            'iu_nd_aval',
            'iu_nd_amont',
            'geo_json',
            'lon',
            'lat'
        ]

        df_node = df_node[col_table_db]
        df_node = df_node.set_index("iu_ac")
        df_node = df_node.loc[~df_node.index.duplicated(keep='first')]

        return df_node

    def _traffic_data_from_file(self, file_to_load):
        """
        Load the traffic date from a given file.

        Augment the data with date information.

        :param file_to_load:
        :return: pandas data frame containing the information.
        """
        df_traffic_data = pd.read_csv(TRAFFIC_DATA / file_to_load, delimiter=";")

        df_traffic_data = df_traffic_data[self._traffic_relevant_columns]
        df_traffic_data = df_traffic_data.rename(columns={'etat_trafic': 'etat_traffic'})

        df_traffic_data["t_1h"] = pd.to_datetime(df_traffic_data["t_1h"])
        dt = df_traffic_data["t_1h"].dt

        df_traffic_data["year"] = dt.year
        df_traffic_data['month'] = dt.month
        df_traffic_data['day'] = dt.day
        df_traffic_data['hour'] = dt.hour

        col_table_db = ['day',
                        'etat_barre',
                        'etat_traffic',
                        'hour',
                        'iu_ac',
                        'k',
                        'month',
                        'q',
                        'year']

        df_traffic_data = df_traffic_data[col_table_db]
        return df_traffic_data

    def traffic_info_to_sql_table(self, engine):

        for file_to_load in tqdm.tqdm(self._traffic_files):
            print(f"→ Loading   {file_to_load}")
            df_traffic_data = self._traffic_data_from_file(file_to_load)
            print(f"→ Loaded    {file_to_load}")

            print(f"→ Injecting {file_to_load}")
            df_traffic_data.to_sql('traffic_info', con=engine, if_exists='append',
                                   index=False,
                                   index_label="objectid")
            print(f"→ Injected {file_to_load}")

    def node_to_sql_table(self, engine):
        self._df_node.to_sql('node', con=engine, if_exists='append', index_label="iu_ac")

    def _load(self, year, week_of_year):
        file_to_load = list(filter(lambda f: ("{}_W{:02d}".format(year, week_of_year) in f), self._traffic_files))[0]

        return self._traffic_data_from_file(file_to_load)

    def get_traffic_data_of_date(self, date_string, running_traffic_only=False):
        """
        Returns the data for a given date_time

        :param running_traffic_only:
        :param date_string: string like "2018-01-01"
        :return: dict of hours ("17:00"-like) to pandas df containing info about the traffic
        """

        date_time_request = datetime.datetime.strptime(date_string, "%Y-%m-%d")
        year = date_time_request.year
        week_of_year = date_time_request.isocalendar()[1]

        df_traffic_data_raw = self._load(year, week_of_year)

        # TODO : put options here
        df_traffic_filtered = df_traffic_data_raw.loc[df_traffic_data_raw.day == date_time_request.day]

        if running_traffic_only:
            df_traffic_filtered = df_traffic_filtered.loc[df_traffic_filtered["etat_barre"] == 1]

        df_joined = df_traffic_filtered.join(self._df_node, on='iu_ac')

        # Loading serialized geo_json
        df_joined.geo_json = df_joined.geo_json.apply(json.loads)

        return df_joined

    def get_estimated_traffic(self, q_lon, q_lat, date_time_string):
        raise NotImplementedError()

    @staticmethod
    def compute_k_q(df_traff, tree: KDTree, q_points, K=100):
        """
        Compute K and q of points in (easting, northing)
        using data of the traffic contained in df_traff.

        Use a mean over KNN of the points.

        :param df_traff: pd.Dataframe containing info of the traffic
        :param tree: kd.Tree containing points related to traffic info
        :param q_points: query points in
        :param K: number of nearest neighbours for the estimations
        :return: estimates of k and q
        """

        # of size (n, K)
        points_neigh_indices = tree.query(X=q_points, k=K, return_distance=False)

        ks = df_traff.k.values
        qs = df_traff.q.values

        k_estimates = np.take(ks, points_neigh_indices).mean(axis=1)
        q_estimates = np.take(qs, points_neigh_indices).mean(axis=1)

        return np.vstack([k_estimates, q_estimates]).T

    def add_traffic_parameters_estimation(self, df, K=100):
        """
        Add estimations of traffic metrics in the data at the
        departure and intervention point.

        :param df: pd.DataFrame
        :param K: K in KNN
        :return: pd.DataDrame
        """

        original_df_index = df.index

        weeks_of_year = sorted(df.week_of_year.unique())

        list_df_estimates = []
        for w_o_y in weeks_of_year:
            df_week = df[df.week_of_year == w_o_y]
            logging.info(f"  → Processing week {w_o_y}: Shape {df_week.shape}")

            df_traffic = self._load(2018, w_o_y)

            # "État ouvert ou non (barré, inconnu ou invalide) à la circulation de l'arc"
            #  ⋅ 0: Inconnu
            #  ⋅ 1: Ouvert
            #  ⋅ 2: Barré
            #  ⋅ 3: Invalide

            df_traffic = df_traffic[df_traffic["etat_barre"] == 1]
            df_joined = df_traffic.join(self._df_node, on='iu_ac', how="inner")
            df_joined = df_joined.dropna()

            def points_in_utm(lats, lons):
                """
                From latitudes longitudes, return
                a np.array of points in eastings, northings.

                Advantages : they are represented using distance in meters.

                :return:
                """
                eastings, northings, _, _ = utm.from_latlon(lats, lons)

                points = np.array([eastings, northings]).T
                return points

            # To efficiently computed estimates, we use a KDTree
            traffic_points = points_in_utm(df_joined.lat.values,
                                           df_joined.lon.values)

            traffic_tree = KDTree(traffic_points, leaf_size=50)

            # Estimates of k and q at departure
            departure_points = points_in_utm(df_week["latitude before departure"].values,
                                             df_week["longitude before departure"].values)

            k_q_depart = self.compute_k_q(df_joined, traffic_tree, departure_points, K=K)

            df_k_q_depart = pd.DataFrame(k_q_depart, columns=['k_departure', 'q_departure'],
                                         index=df_week.index)

            # Estimates of k and q at intervention
            intervention_points = points_in_utm(df_week["latitude intervention"].values,
                                                df_week["longitude intervention"].values)

            k_q_inter = self.compute_k_q(df_joined, traffic_tree, intervention_points, K=K)

            df_k_q_inter = pd.DataFrame(k_q_inter, columns=['k_intervention', 'q_intervention'],
                                        index=df_week.index)

            # Merging estimates with the weekly dataset
            df_weekl_and_estimates = df_week.join(df_k_q_depart, how="left").join(df_k_q_inter, how="left")

            logging.info(f"  → Shape {df_weekl_and_estimates.shape}")

            list_df_estimates.append(df_weekl_and_estimates)

        # Merging all
        full_transformed_df = pd.concat(list_df_estimates)

        # Sorting as it was originally
        full_transformed_df = full_transformed_df.loc[original_df_index]
        return full_transformed_df


class SQLTrafficInterface(TrafficInterface):
    """
    An concrete interface to database that uses SQLAlchemy.

    WIP
    """

    def __init__(self):
        super().__init__()
        self._engine = create_engine(get_db_string())
        self._session_maker = sessionmaker(self._engine)

    def get_estimated_traffic(self, q_lon, q_lat, date_time_string):
        """
        Return metrics of traffic (k,q) at a given point for a given date_time_string.

        It request TrafficInfo data
        for a given day in the local neighborhood of a point

        Really slow (especially with SQLite)

        WIP

        :param q_lon:
        :param q_lat:
        :param date_time_string:
        :return:
        """

        date_time_request = datetime.datetime.strptime(date_time_string, DATE_FORMAT_DATASET)
        q_year = date_time_request.year
        q_month = date_time_request.month
        q_day = date_time_request.day
        q_hour = date_time_request.hour

        # Define the neighboorhood box
        d_lon = 0.005
        d_lat = 0.005

        session = self._session_maker()

        nodes = session.query(Node) \
            .filter(q_lat - d_lat < Node.lat).filter(Node.lat < q_lat + d_lat) \
            .filter(q_lon - d_lon < Node.lon).filter(Node.lon < q_lat + d_lon)

        print(len(list(nodes)))

        print(2)
        q = session.query(TrafficInfo).join(nodes, TrafficInfo.iu_ac == nodes.c.iu_ac) \
            .filter(TrafficInfo.year == q_year) \
            .filter(TrafficInfo.month == q_month) \
            .filter(TrafficInfo.day == q_day) \
            .filter(TrafficInfo.hour == q_hour)

        print(len(list(q)))

    def get_traffic_data_of_date(self, date_string, running_traffic_only=False):
        raise NotImplementedError()


class ClickableMap(Map):
    """
    Simplified map interaction using ipyleaflet

    Have a look at: http://leaflet.github.io/Leaflet.draw/docs/examples/basic.html

    """

    def __init__(self, center=[48.8566, 2.3522],
                 zoom=13,
                 layout=ipywidgets.Layout(width='100%', height='800px')):
        super().__init__(center=center, zoom=zoom, layout=layout, scroll_wheel_zoom=True)

        # Initialize an action counter variable
        self.actionCount = 0
        self.AOIs = []

        # Using Custom Basemap
        self.clear_layers()
        mosaicsTilesURL = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
        self.add_layer(TileLayer(url=mosaicsTilesURL))

    def add_geojson(self, geo_json, style=None):
        """
        Custom function to create and add a Polygon layer
        :param json: geo_json
        :param kwargs:
        :return:
        """

        if style is None:
            style = {}

        r = GeoJSON(data=geo_json, style=style)
        self.add_layer(r)
        return self


def show_itinary(lon_start, lat_start, lon_end, lat_end, profile="car"):
    """
    Return a IFrame to show the itinary
    """

    src = f'https://map.jjerphan.xyz/?z=13&loc={lat_start}%2C{lon_start}&loc={lat_end}%2C{lon_end}&hl=en&alt=0'

    return IFrame(src, width="1700", height="1000")


def get_itinary(lon_start, lat_start, lon_end, lat_end, profile="car", with_details=False):
    """
    Return the itinary from theOSMR server
    """

    src = f'https://{profile}.osrm.jjerphan.xyz/route/v1/driving/{lon_start},{lat_start};{lon_end},{lat_end}'
    if with_details:
        src += "?overview=false&alternatives=true&steps=true"

    r = requests.get(src)
    return r.json()


def plot_on_map(lons_samples, lats_samples):
    """
    Return a map of points given their coordinates
    """
    m = ClickableMap()

    lon_mean = np.mean(lons_samples)
    lat_mean = np.mean(lats_samples)

    for lon, lat in zip(lons_samples, lats_samples):
        geo_json = {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [lon, lat]
            },
            "properties": {}
        }

        m.add_geojson(geo_json)

    m.center = [lat_mean, lon_mean]
    return m


if __name__ == "__main__":
    # print("Creating database")
    # db_string = get_db_string()
    # engine = create_engine(db_string, echo=True)
    # Base.metadata.create_all(engine)
    # csv_to_sql()

    ti = CSVTrafficInterface()

    # d = ti.get_traffic_data_of_date("2018-07-08")
    registry = SessionRegistry()
    datasets_pair_folder, X_train, _ = registry.select_datasets_pair()

    # ti.add_traffic_parameters_estimation()


