#!/usr/bin/env python
# coding: utf-8

import json
import logging

import numpy as np
import pandas as pd
import polyline

# Do not remove
import swifter

import utils
from settings import INDEX_COLUMN, TO_PREDICT, X_TRAIN_ADD, X_TRAIN, X_TEST, X_TEST_ADD, DATA_FOLDER, n_test, n_train, \
    OSRM_ESTIMATES_CSV, alert_reason_category_name
from interfaces import CSVTrafficInterface, get_itinary
from utils import load_x, load_ytrain, convert_time, replace_missing_databy_median

logging.basicConfig(level=logging.INFO)
pd.options.mode.chained_assignment = "raise"

FEATURES_TO_DROP = [
    "GPS tracks departure-presentation",
    "GPS tracks datetime departure-presentation",

    # included in delta position gps previous departure-departure
    "OSRM response",
    "intervention",
    "alert reason",
    "location of the event",
    "emergency vehicle",
    "emergency vehicle type",
    "rescue center",
    "selection time",
    "date key sélection",
    "time key sélection",
    "delta status preceding selection-selection",
    "delta position gps previous departure-departure",
    "time elapsed between selection and last observed GPS position",
    "status preceding selection",

    "updated OSRM estimated duration",
    "OSRM estimated distance from last observed GPS position",
    "OSRM response",
    "OSRM estimate from last observed GPS position",
    "OSRM estimated distance",
    "OSRM estimated duration",
    "OSRM estimated duration from last observed GPS position",

    "year",
    "week_of_year",
    "month",
    "day",
    "hour",
    "day_of_week",
    "date",

    # "intervention on public roads",
    # "boat",
    # "pedestrians",
    # "ambulance",
    "size fleet",
    "absolute rank",

    # Removed of this list if using `add_post_intervention_features`
    "floor"
]


def date_to_timestamp(string_date, format='%Y%m%d'):
    """
    Quick utils functions to convert
    :return:
    """
    return pd.to_datetime(string_date, format=format).timestamp()


vacances = [
    # Début, fin (dernier jour inclu)
    (date_to_timestamp('20180101'), date_to_timestamp('20180109')),
    (date_to_timestamp('20180217'), date_to_timestamp('20180306')),
    (date_to_timestamp('20180414'), date_to_timestamp('20180502')),
    (date_to_timestamp('20180707'), date_to_timestamp('20180904')),
    (date_to_timestamp('20181020'), date_to_timestamp('20181106')),
    (date_to_timestamp('20181222'), date_to_timestamp('20190101'))
]


def est_vacances(t):
    """
    Add a boolean to indicate if it is national holiday.

    :param t:
    :return:
    """
    for (start, end) in vacances:
        if start <= t.timestamp() <= end:
            return True
    return False


def est_ferie(t):
    """
    Add a boolean to indicate if it is national holiday.

    :param t: datetime
    :return:
    """
    feries = pd.to_datetime(['20180101', '20180402',
                             '20180501', '20180508',
                             '20180510', '20180521',
                             '20180714', '20180815',
                             '20181101', '20181225',
                             '20181111'], format='%Y%m%d')
    return t.date() in feries


def add_time_representation(df: pd.DataFrame) -> pd.DataFrame:
    """
    Add a time representation for the datetime

    :param df: data frame containing a Datetime series named `selection` time
    :return:
    """
    logging.info(" → STEP: Add Time Features")

    df["selection time"] = pd.to_datetime(df["selection time"])
    dt = df["selection time"].dt

    df["year"] = dt.year
    df["week_of_year"] = dt.weekofyear
    df["month"] = dt.month
    df["day"] = dt.day
    df["hour"] = dt.hour
    df["day_of_week"] = dt.weekday

    # Circular representation
    df['month_sin'] = np.sin((dt.month - 1) * (2. * np.pi / 12))
    df['month_cos'] = np.cos((dt.month - 1) * (2. * np.pi / 12))
    df['day_sin'] = np.sin((dt.day - 1) * (2. * np.pi / 31))
    df['day_cos'] = np.cos((dt.day - 1) * (2. * np.pi / 31))

    df['minute_in_day_sin'] = np.sin((dt.hour * 60 + dt.minute) * (2. * np.pi / 1440))
    df['minute_in_day_cos'] = np.cos((dt.hour * 60 + dt.minute) * (2. * np.pi / 1440))

    df["ferie"] = df["selection time"].apply(est_ferie).astype(int)

    df["vacances"] = df["selection time"].apply(est_vacances).astype(int)

    return df


def one_hot_encode_n_most_frequent_values(df: pd.DataFrame, colname: str, n: int = 5) -> pd.DataFrame:
    """
    One hot encode the n most frequent values of a categorical column.
    A column dedicated to other value is also encoded.

    :param df: the DataFrame
    :param colname: the name of the categorical column to encode

    """
    logging.info(f" → STEP: Encode {colname} on its {n} most frequent values ")

    less_freq_vals = df[colname].value_counts()[n:].index
    most_freq_vals = df[colname].value_counts()[:n].index

    res = pd.get_dummies(df[colname].replace(less_freq_vals, f'{colname}_others'))
    new_names = [f"{colname}_{value}" for value in most_freq_vals]
    res = res.rename(columns=dict(zip(most_freq_vals, new_names)))

    df = df.join(res, how="left", on=INDEX_COLUMN)

    return df


def remove_outliers(df: pd.DataFrame) -> pd.DataFrame:
    """
    Drop outliers based on delta times.

    We know that outliers are meaningless (due to human oversight)
    arbitrary threshold, subject to change.
    """
    logging.info(" → STEP: Drop outliers")

    df = df[df['delta selection-departure'] < 1800]
    df = df[df['delta departure-presentation'] < 3600]
    return df


def add_newer_osmr_response(df: pd.DataFrame) -> pd.DataFrame:
    """
    Add adapted OSMR estimation based on the type of vehicles.

    Relates in some ways to `add_vehicule_info`.

    :param df:
    :return:
    """
    logging.info(" → STEP: OSMR and transit features")

    osmr_infos = pd.read_csv(OSRM_ESTIMATES_CSV, index_col="emergency vehicle selection")
    osmr_infos = osmr_infos.drop(columns=["longitude before departure", "latitude before departure",
                                          "longitude intervention", "latitude intervention"])

    df = df.join(osmr_infos, on="emergency vehicle selection", how="left")

    return df


def add_vehicle_info(df: pd.DataFrame) -> pd.DataFrame:
    """
    Add vehicle information as one-hot-category in:
        ⋅ light vehicles
        ⋅ heavy vehicles
        ⋅ very heavy vehicles
        ⋅ pedestrians
        ⋅ boats
        ⋅ ambulances

    :param df:
    :return:
    """
    logging.info(" → STEP: Add vehicle information")

    vehicle_infos = pd.read_csv(DATA_FOLDER / "vehicles_info.csv", index_col="emergency vehicle type")
    vehicle_infos = vehicle_infos.drop(columns=["name"])

    df = df.join(vehicle_infos, on="emergency vehicle type", how="left")

    return df


def add_post_intervention_features(df: pd.DataFrame) -> pd.DataFrame:
    """
    Add features that are given after the intervention.

    This gives better performances but does not
    make sense when predicting for real new interventions

    :param df:
    :return:
    """
    logging.info(" → STEP: Add features given after intervention")

    df["GPS tracks count"] = df["GPS tracks departure-presentation"].apply(
        lambda x: 0 if pd.isnull(x) else len(x.split(";"))
    )

    for feature_to_keep in ["floor"]:
        FEATURES_TO_DROP.remove(feature_to_keep)

    return df


def add_traffic_estimation(df: pd.DataFrame, K=100) -> pd.DataFrame:
    """
    Add estimation at the starting and interventions coordinates of:
        ⋅ k, the occupancy rate, which corresponds to the
        time of presence of vehicles on the loop as a
        percentage of a fixed time interval (one hour
        for the data provided).  This rate provides
        information on traffic congestion.

        ⋅ q, the flux, which corresponds to the number of vehicles
        that have passed the counting point during a fixed
        time interval (one hour for the data provided).

    Estimations are made on the year 2018 via K-NN estimation for K=100.
    Estimations are given based on roads with running traffic.

    See `CSVTrafficInterface` for more information.

    :param df:
    :param K:
    :return:
    """
    logging.info(" → STEP: Estimate K and q from Traffic")

    df = CSVTrafficInterface().add_traffic_parameters_estimation(df, K=K)
    return df


def add_fleet_information(df: pd.DataFrame) -> pd.DataFrame:
    """
    Add information about each intervention fleet.

    Indeed, for one fleet, several vehicles can be called.
    This add:
     ⋅ the number of vehicles for the intervention (integer)
     ⋅ the absolute rank of vehicles for the intervention (integer)
     ⋅ the relative rank of vehicles for an intervention (float in [0, 1]


    :return:
    """
    logging.info(" → STEP: Add counter and rank of vehicle during intervention")

    df["size fleet"] = (
        df["intervention"].value_counts().loc[df["intervention"]].values
    )

    absolute_rank = (
        df.sort_values(["selection time"], ascending=True)
            .groupby(["intervention"])["emergency vehicle"]
            .rank()
            .rename("absolute rank")
    )

    df = df.join(absolute_rank, how="left")

    df["relative rank"] = (df["absolute rank"]) / df["size fleet"]

    return df


def add_weather(df: pd.DataFrame) -> pd.DataFrame:
    """
    Add information about weather.

    See meteo for more information
    Data given by MeteoFrance:
    https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=90&id_rubrique=32

    :return:
    """
    logging.info(" → STEP: Add weather information")
    df['date'] = df[['date key sélection', 'time key sélection']].apply(convert_time, axis=1).astype('int64')

    l_meteo = []
    for month in range(1, 13):
        month_meteo = pd.read_csv(DATA_FOLDER / ('meteo/synop.2018%s.csv' % (str(month).zfill(2))), sep=';')

        # We only keep data from the Orly Station
        # https://donneespubliques.meteofrance.fr/donnees_libres/Txt/Synop/postesSynop.csv
        orly_id = 7149
        month_meteo = month_meteo[month_meteo["numer_sta"] == orly_id]

        l_meteo.append(month_meteo)
    data_meteo = pd.concat(l_meteo)

    useful_features = ['date', 'rafper', 't', 'u', 'ht_neige', 'rr12', 'vv']
    data_meteo = data_meteo[useful_features]

    data_meteo = data_meteo.apply(replace_missing_databy_median, axis="index")

    data_meteo = data_meteo.rename(columns={
        "rafper": "vent",
        "t": "temperature",
        "u": "humidite",
        "ht_neige": "neige",
        "rr12": "pluie",
        "vv": "visibilite"
    })

    # We need both values to be sorted to perform the merge
    # We keep both index and revert the situation then
    original_index = df.index
    df_sorted = df.sort_values(by=['date'])
    sorted_index = df_sorted.index
    data_meteo = data_meteo.sort_values(by=['date'])

    # Merge to closest datetime
    df_merged = pd.merge_asof(df_sorted, data_meteo, on="date")

    # Resetting previous index
    df_merged = df_merged.set_index(sorted_index).loc[original_index]

    return df_merged


def preprocess_dataset(df: pd.DataFrame) -> pd.DataFrame:
    """
    Perform the preprocessing of the dataset
    containing both values for the training and testing sets

    :param df: pd.DataFrame
    :return: pf.DataFrame
    """

    df = add_time_representation(df)

    assert len(df) == n_train + n_test

    df = add_vehicle_info(df)

    assert len(df) == n_train + n_test

    df = add_newer_osmr_response(df)

    assert len(df) == n_train + n_test

    df = add_weather(df)

    assert len(df) == n_train + n_test

    # df = add_post_intervention_features(df)

    # assert len(df) == n_train + n_test

    df = add_traffic_estimation(df)

    assert len(df) == n_train + n_test

    df = add_fleet_information(df)

    assert len(df) == n_train + n_test

    # df = one_hot_encode_n_most_frequent_values(df, colname="alert reason", n=5)

    # assert len(df) == n_train + n_test

    logging.info(" → STEP: Encode alert category")
    df = pd.get_dummies(df, columns=["alert reason category"])

    logging.info(" → STEP: Drop heavy columns")
    df.drop(FEATURES_TO_DROP, axis=1, inplace=True)

    return df


if __name__ == "__main__":
    """
    Script for preprocessing:
     ⋅ Load and merge x_train and x_test
     ⋅ Perform (long) preprocessing
     ⋅ Drop unused features
     ⋅ Save the data set pair (X_train, X_test)
    :return:
    """

    # Loading both dataset: we add a column named 'test'
    # identify whether an observation belong to
    # the testing set or not

    logging.info(" → STEP: Loading data sets")

    df = load_x(X_TRAIN)
    additional = load_x(X_TRAIN_ADD)
    df = df.merge(additional, how="left", on=INDEX_COLUMN)
    y = load_ytrain()
    train = df.merge(y, how="left", on=INDEX_COLUMN)
    train["test"] = False

    df = load_x(X_TEST)
    additional = load_x(X_TEST_ADD)
    test = df.merge(additional, how="left", on=INDEX_COLUMN)

    test["test"] = True

    df = pd.concat((train, test), sort=False)

    logging.info(" → Started pre-processing")

    # The long preprocessing
    df = preprocess_dataset(df)

    is_test = df["test"]

    df.drop(["test"] + TO_PREDICT, axis=1, inplace=True)

    # Formatting columns name
    df.columns = df.columns.str.lower().str.replace(' ', '_')
    df = df.rename(columns=alert_reason_category_name)

    logging.info(" → SUMMARY: Full dataset (X_train and X_test)")
    df.info()

    X_train = df[~is_test]
    X_test = df[is_test]

    session_reg = utils.SessionRegistry()

    session_reg.save_datasets_pair(X_train, X_test)
