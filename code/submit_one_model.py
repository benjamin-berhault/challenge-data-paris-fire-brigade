#!/usr/bin/env python
# coding: utf-8

import pandas as pd

from utils import SessionRegistry, create_submission_delta_selection_presentation

if __name__ == "__main__":
    """
    Script for submission

      ⋅ Let the user select the model for the preparation time
      ⋅ Let the user select the model for the transit time
      ⋅ Predict preparation time on test data
      ⋅ Predict transit time on test data
      ⋅ Create a submission with predictions

    :return:
    """
    registry = SessionRegistry()

    print("Select model for delta_selection_presentation")

    model, _, X_test = registry.select_session()
    X_test = X_test.fillna(X_test.median())

    delta_prep = pd.DataFrame(model.predict(X_test))

    index_test = pd.DataFrame(X_test.index)
    create_submission_delta_selection_presentation(emergency_vehicule_selection=index_test,
                                                   delta_selection_presentation=delta_prep)
