\section{Introduction: définition du problème}
\label{problem_definition}

Le temps de réponse est l'un des facteurs les plus importants pour les services d'urgence, car leur capacité à sauver des vies et à secourir les gens en dépend. Un choix non optimal d'un véhicule d'urgence pour une demande de secours peut allonger le temps d'arrivée des sauveteurs et avoir un impact sur l'avenir de la victime. Ce choix est donc très critique pour les services d'urgence et dépend directement de leur capacité à prévoir avec précision l'heure d'arrivée des différentes unités disponibles. \\

Le but de ce challenge est de prédire, à l'aide de descripteurs des interventions précédentes, le temps de réponse des véhicules de la Brigade des Pompiers de Paris pour des interventions nouvelles. Dans cette partie, nous rappelons la formalisation du problème (section \ref{ssec:donnes-a-predire}), puis nous fournissons une vue d'ensemble des données fournies\ref{uml}, avant de discuter la métrique à optimiser et l'intérêt du challenge pour la minimisation des temps de trajets (section \ref{ssec:disscussion}).

\subsection{Données à prédire : temps d'intervention}
\label{ssec:donnes-a-predire}
Le temps de réponse des véhicules de la Brigade des pompiers de Paris est le délai entre:

\begin{itemize}
	\item la sélection d'un véhicule de secours, moment où une équipe de secours est avertie
	\item l'arrivée de l'équipe de secours sur les lieux de l'intervention
\end{itemize}

Ce temps de réponse se décompose en deux périodes à prédire:

\begin{itemize}
	\item \dsd la période de préparation de l'équipe de secours
	\item \ddp la période de transit sur la route de l'équipe de secours
\end{itemize}

\subsection{Choix de la métrique $R^2$ du challenge et motivation vis-à-vis de l'objectif global}
\label{ssec:disscussion}

La réussite d'un challenge de prédiction se fonde avant tout sur la capacité des participants à mettre en place une méthode d'évaluation appropriée. Dans ce cadre, connaître la métrique et la manière dont sont évaluées les prédictions est primordial, c'est ce que nous explicitons dans cette partie.

\subsubsection{Définition du coefficient de détermination}

La métrique utilisée pour évaluer les prédictions est le $R^2$, parfois appelé également coefficient de détermination. Soit $(\hat{y}_i)_{i=1}^n$ les prédictions et $(y_i)_{i=1}^n$ les observations effectives et $\overline{y}$ leur moyenne empirique, on définit le terme de variance totale:
$$
SS_{tot} = \frac{1}{n} \sum_{i=1}^n (y_i - \overline{y})^2
$$

et erreur quadratique moyenne (abrégée dans la suite MSE pour "mean squarred-error") comme:

$$
MSE = \frac{1}{n} \sum_{i=1}^n (y_i - \hat{y}_i)^2
$$

On definit enfin le coefficient de détermination comme la comparaison de 1 au ratio de ces deux termes de variance:

\begin{equation}
	R^2 = 1 - \frac{MSE}{SS_{tot}}
\end{equation}

Ainsi, il faut donc réaliser une prédiction au moins aussi bonne que la prédiction constante de moyenne pour obtenir un score positif.

\subsubsection{Choix du coefficient de détermination et autres métriques}

Le choix du $R^2$ comme métrique a été réalisé par la brigade pour ses valeurs prises usuellement prises dans $[0,1]$ -- même si de fait les prédictions peuvent aussi être négatives arbitrairement grandes en valeur absolue -- et donc son interprétabilité moins intuitive. Un score de $0.3$ signifie que l'on a une diminution de $70\%$ de variance dans la prédiction par rapport à la variance des données. \\

On peut se demander si utiliser l'erreur quadratique moyenne (MSE) ne serait pas plus interprétable : typiquement la racine carrée de l'erreur quadratique moyenne (RMSE) nous donne directement une déviation en seconde concerant l'arrivée, ce qui pourrait être un indicateur plus utilisable. De même, la MSE est généralement populaire car elle peut être à la fois utilisée comme fonction de score et comme fonction d'optimisation des algorithmes du fait de sa convexité.\\

Si le score final est donné sur le temps total d'intervention \texttt{delta\_selection\_presentation}, dans la suite de notre analyse, le $R^2$ et la MSE seront tous deux utilisés pour attester des performances lors de l'apprentissage des modèles.

\subsubsection{Évaluation des résultats sur la plateforme \emph{Challenge Data} de l'École Normale Supérieure}

Du fait de la soumission de prédictions pour les trois variables, il est important de connaître la manière dont les celles-ci sont évaluées pour mieux interpréter les résultats. \\

Dans le cadre de ce challenge, l'évaluation du score final est réalisée avec la métrique \texttt{r2\_score} de \texttt{scikit-learn} sur le tableau de ces trois prédictions. Ceci correspond donc à la moyenne des trois scores coefficients de détermination pour chacune des variables.