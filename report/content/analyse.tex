\section{Connaissance du domaine et analyse exploratoire du jeu de données}

Une connaissance du domaine d'étude, une compréhension fine des variables explicatives, et la constitution appropriée d'une représentation de ces variable est généralement ce qui prime sur la réussite d'un challenge \cite{methodecompet} : il s'agit d'exploiter au maximum les données fournies, pour produire des modèles aux prédictions les plus exactes possibles ; c'est ce nous présentons dans cette section. \\

Nous exposerons ici le retour de notre rencontre avec deux membres de la brigade de Paris qui expliquent le déroulement général d'une intervention dans \ref{ssec:metier}, puis nous dresserons une étude pour chacune des variables du jeu de données ainsi que les dérivations pertinentes qui peuvent être faîtes dans le cadre de ce challenge dans \ref{ssec:uni}.

\subsection{Retour sur la rencontre avec le personnel de la BSPP : connaissances métiers}
\label{ssec:metier}

Afin d'avoir une compréhension plus holistique du problème et de son cadre — et de manière plus formelle, afin de construire une représentation des données adaptée pour l'apprentissage de modèles — nous avons voulu tout d'abord en apprendre plus sur l'organisation des services de secours de la Brigade des Sapeurs Pompiers de Paris. Pour ce faire, nous avons rencontré le jeudi 13 février 2018, Benjamin Berhault, \emph{Data Architect} et responsable du challenge, et Julien Vorières, \emph{Business Analyst}, tous deux anciens membres d'équipes d'interventions pendant plusieurs années. Ces membres de la \emph{BSPP} ont pu répondre à nos interrogations quant au traitement des appels et aux déroulement des interventions. Ils nous ont aussi indiqué les paramètres qui leur semblaient les plus importants dans le temps de réponse des équipes de secours. De ces échanges, de nombreuses informations pertinentes sont à retenir. \\

Tout d'abord, les services de secours et de sécurité s'orientent vers un numéro d'urgence unique au niveau européen : le 112. En pratique, tous les appels aux pompiers ou à la police sont traités au même centre ; à Paris, il s'agit du centre d'appels de la BSPP. Chaque appel est traité selon une série de questions qui suivent un arbre de décision. La personne décide ensuite de la redirection de l'appel vers le bon service. Dans le cas d'un appel nécessitant la venue de pompiers, entre une et deux minutes en moyenne sont passées pour prendre la décision de l'envoi d'une équipe donnée. \\

Ensuite, le temps de départ dépend grandement de la situation dans laquelle les équipes de secours se trouvent : par exemple la nuit, les pompiers doivent s'équiper avant de partir. De même, les feux demandent un équipement de plus en plus spécialisé, qui peut donc prendre du temps pour sa mise en place. \\

De plus , la capacité de réponse d'une équipe de secours dépend essentiellement du le conducteur et du copilote qui l'assiste durant le trajet. Le guidage se fait essentiellement grâce à l'expérience qui use d'une connaissance fine des voies et de leurs accessibilité aux regards des dimensions du véhicule réquisitionné. Dans certains cas plus rares, le guidage du véhicule se fait à l'aide d'un GPS. Lorsqu'il y a des cas d'intempéries, celles-ci peuvent compliquer le trajet. Enfin, si une intervention nécessitent plusieurs véhicules, les premiers arrivent généralement bien plus rapidement sur les lieux de l'intervention. La congestion des voies amène le plus souvent des retards.\\

De même, les différents temps récupérés correspondent à des activations manuelles faites par le responsable lors du départ et de l'arrivée. Les temps aberrants  -- ceux extrêmement longs, comme par exemple cinq heures pour le choix d'un véhicule -- correspondent à des oublis de cette activation manuelle : exclure les valeurs aberrantes est donc tout à fait sensé ici. Une autre pratique courante consiste à indiquer la présentation sur l'intervention une fois que le véhicule est garé pour ensuite se concentrer uniquement sur la victime à secourir. Dans ce cas, l'information de l'étage n'influence pas la durée d'intervention par exemple. \\

Enfin, certains descripteurs du jeu de données sont uniquement disponibles post-intervention, cela est notamment le cas des coordonnées GPS du véhicule, de l'étage ou encore du type de lieu de l'intervention. \\

Plus d'informations sur l'organisation des interventions sont données en annexes \ref{ann:orga}. 

\subsection{Présentation et analyse univariée du jeu de données du challenge}
\label{ssec:uni}

Ce jeu de données se décompose en deux parties canoniques principales \texttt{x\_train.csv} et \texttt{y\_train.csv}. \\

Les observations de \texttt{x\_train.csv} correspondent aux données de la sélection d'un véhicule pour l'intervention. Ces données sont indexées par un identifiant \texttt{emergency vehicle selection}. Chacune de ces observations comporte 24 descripteurs relatifs à la nature de l'intervention et au transit des véhicules vers chaque site d'intervention. \\

Un récapitulatif de ce jeu de données est donné sur le diagramme UML \ref{uml}\footnote{Pour plus d'information sur le formalisme de modélisation qu'est UML, on peut se référer à \cite{uml}.}. 

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{diag}
	\caption{Modélisation du jeu de données des interventions}
	\label{uml}
\end{figure}


Les éléments de \texttt{y\_train.csv} correspondent aux périodes de temps de préparation et de transits — décrites en \ref{ssec:donnes-a-predire} page \pageref{ssec:donnes-a-predire} — observées pour chacune des sélections de véhicules présentées par \texttt{x\_train.csv}. \\

Ce jeu de données est décrit explicitement en ligne sur le site du challenge \footnote{Voir ici: \url{https://paris-fire-brigade.github.io/data-challenge/challenge.html}}. \\

Nous dressons maintenant une analyse succincte des différentes variables de paramètres, avec les choix possibles pour leur utilisation, en gardant l'ordre proposé dans le challenge. Pour des analyses univariées et multivariées avancées de ces variables, on peut retrouver l'ensemble de nos études en annexes et dans les \emph{Jupyter notebooks} attachés à ce projet.

\newpage
\subsubsection{Temps de préparation et de temps de transit}

On s'intéresse tout d'abord aux variables à expliquer, après en avoir exclus les points aberrants.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{hist/delta}
	\caption{Distribution empirique du temps de préparation (en bleu) et de transit (en orange)}
	\label{delta}
\end{figure}

Les deux distributions obtenues sont quasiment gaussiennes comme l'illustre la figure \ref{delta}. D'un côté, les temps de préparation sont plutôt courts, avec une moyenne de $130$ secondes, tandis que les temps d'interventions sont plus conséquents et disposent également d'une plus grande variance. On voit cependant que la contribution du temps de sélection ne peut pas être considérée comme négligeable vis-à-vis du temps global d'intervention.

\subsubsection{Identification des interventions}

Pour savoir à quoi correspond une intervention, nous disposons essentiellement de trois paramètres
\begin{description}
	\item[intervention] Identifiant unique du problème qui motive l'envoi de secours
	\item[alert reason category] un entier de 1 à 9 rassemblant de grandes familles d'événements 
	\item[alert reason] Sous catégorie des catégories précédentes, qui correspondent à des types d'interventions spécifiques
\end{description}

Premièrement, le paramètre intervention n'a pas de sens propre, il sert uniquement à identifier le problème de façon unique. Néanmoins, on peut en déduire des informations pertinentes, à savoir combien de véhicules au total seront envoyés sur l'intervention, et l'ordre de leur envoi — puisqu'on dispose du temps de départ de chacun. On infère donc ces deux informations pour les inclure dans la représentation de nos données pour l'apprentissage de nos modèles. Prendre en compte directement cette variable n'a cependant pas de sens : nous l'excluons donc de l'analyse. \\

De plus, le temps d'intervention selon les deux descripteurs \textbf{alert reason category} et \textbf{alert reason} a une grande variabilité comme illustré sur les boîtes à moustaches respectives \ref{delta_departure-presentation_by_alert_reason_category} page \pageref{delta_departure-presentation_by_alert_reason_category} et \ref{delta_selection-departure_by_alert_reason} page \pageref{delta_selection-departure_by_alert_reason}. Ces résultats suivent l'intuition que la nature des interventions modifie la réaction des équipes de secours. À titre indicatif, les catégories de raisons d'alertes sont présentées dans le tableau \ref{raisons}. Il est donc crucial de prendre en compte ces descripteurs dans les modèles, puisqu'ils donnent indirectement le niveau d'urgence et l'équipement nécessaire pour les pompiers, deux facteurs déterminants dans le temps d'intervention.

\begin{table}[H]
\centering
\begin{tabular}{|c|c|}
	\hline 
	Identifiant & Dénomination\\
	\hline
	\hline
	1 & Feu \\
	2 & Accidents, circulation \\
	3 & Secours à victimes \\
	4 & Assistance à personne \\
	5 & Animaux \\
	6 & Eaux, gaz, électricité \\
	7 & Risque technologique \\
	8 & Reconnaissance de personnes \\
	\hline
\end{tabular}
\caption{Catégories de raisons d'alerte}
\label{raisons}
\end{table}

\subsubsection{Lieu d'intervention}

Plusieurs paramètres permettent de connaître  le lieu de l'intervention:
\begin{description}
	\item[intervention on public roads] un booléen selon si l'intervention a lieu directement sur la voie publique (par exemple un malaise en pleine rue)
	\item[floor] un entier désignant l'étage de l'intervention
	\item[location of the event] le lieu précis, tel la cuisine. Ce descripteur est rempli \emph{a posteriori} par l'équipe
	\item[longitude intervention] longitude du lieu d'intervention identifié 
	\item[latitude intervention] latitude du lieu d'intervention identifié
\end{description}

Il est possible que les interventions qui ont lieu directement sur des voies publiques soient un peu plus rapides, car l'équipe peut voir le problème de plus loin (attroupement par exemple autour de la victime) et connait aussi mieux ce type de voies. Nous gardons donc l'information donnée par \textbf{intervention on public roads}. Les descripteurs \textbf{floor} et \textbf{location of the event} par contre ne semblent pas être des paramètres pertinents, du fait qu'ils sont donnés \emph{a posteriori}. Cela est d'ailleurs confirmé par l'analyse de données qui ne met pas en avant de dépendance par rapport à ces variables comme illustré sur les boîtes à moustaches associées \ref{delta_selection-departure_by_floor} page \pageref{delta_selection-departure_by_floor} et \ref{delta_departure-presentation_by_location_of_the_event} page \pageref{delta_departure-presentation_by_location_of_the_event}. Nous les excluons donc de notre étude. Nous gardons \textbf{longitude intervention} et \textbf{latitude intervention} qui portent une information notable pour l'intervention.

\subsubsection{Sur les véhicules}

À nouveau, de nombreuses informations sont disponibles sur les véhicules et sur leur état antérieur à l'intervention. On a ainsi les paramètres suivants:

\begin{description}
	\item[emergency vehicle] l'identifiant unique du véhicule utilisé
	\item[emergency vehicle type] catégorie de véhicule. Il y en a 75 dans le jeu d'entrainement et d'autres inconnus dans le jeu de test
	\item[rescue center] l'identifiant du centre de rattachement du véhicule
	\item[status preceding selection] l'état précédent du véhicule, principalement s'il est rentré ou en retour d'une intervention
	\item[delta status preceding selection-selection] nombre de secondes écoulées depuis l'entrée du statut précédent
	\item[departed from its rescue center] indique si le véhicule est parti de son emplacement de garage
\end{description}

L'identifiant unique du véhicule \textbf{emergency vehicle} n'a pas d'impact a priori, et même s'il existait cela reviendrait essentiellement à discriminer selon les conducteurs, ce qui ne semble ni éthique — car le but n'est pas de rendre encore plus stressant le métier de pompier — ni judicieux — car un jeune conducteur va par exemple sans doute acquérir une plus grande dextérité après quelques mois, par exemple. Nous ne retenons donc pas ce paramètre pour l'étude. \\

Le type de véhicule \textbf{emergency vehicle type} en revanche peut être très intéressant. Le problème est que le descripteur fournit correspond à une catégorisation un peu trop fine de ceux-ci. Par exemple les labels diffèrent pour une ambulance selon son hôpital de rattachement. Une re-catégorisation qui semble judicieuse est de distinguer entre poids lourds et véhicules légers, les seconds étant plus maniables que les premiers, et pouvant avoir des contraintes d'itinéraires dues à des voies trop étroites. \\

On construira donc une catégorisation de ces véhicules et on paramétrera la prédiction GPS via le système de routage en ce sens. Enfin, on distingue les interventions par bateaux et se faisant à pied, qui ne suivent pas les mêmes modes de déplacement, et qu'il faut donc prendre en compte de manière séparée pour la prédiction du temps de transit. \\ 

Enfin, les trois derniers paramètres, c'est-à-dire \textbf{status preceding selection}, \textbf{delta status preceding selection-selection} et \textbf{departed from its rescue center} permettent essentiellement de déduire le niveau de disponibilité du véhicule. Un véhicule déjà en mouvement peut repartir immédiatement et est déjà dans la circulation, donc plus à même de rouler vite. C'est un facteur de réduction des deux temps à calculer. On peut cependant se demander s'il est judicieux de conserver autant de colonnes si la variabilité vient uniquement d'un booléen en intervention ou non en intervention.

\subsubsection{Temps}

L'indication temporelle est fournie via deux paramètres:
\begin{description}
	\item[date key selection] la date représentée par un d'entier de la forme AAAAMMJJ
	\item[time key selection] l'heure représentée par un entier de la forme HHMMSS
\end{description}

Ces deux informations sont évidemment cruciales. On remarque que toutes les données sont issues de 2018 donc il ne sert à rien d'inclure l'année dans le modèle (ceci deviendrait faux si les données étaient sur une période plus grande). Ensuite la représentation donnée n'est pas pertinente puisqu'elle ne rend pas compte du positionnement horaire des enregistrements sur une journée, et du positionnement du jour de l'enregistrement dans le mois. Nous représentons donc l'heure au sein d'un jour par le nombre de minutes ($heure\times 60 + minutes$) projeté sur le cercle unité via les fonctions trigonométriques. L'intérêt de garder une granularité à la seconde ne semble pas évident ; en revanche avoir une granularité à la minute permet de détecter l'organisation de la vie de la caserne. \\ 

La motivation de cette représentation fut en effet confirmée lors de nos rencontre avec les  membres de la brigade qui ont insisté sur certaines considérations temporaires qui retardent leur départ selon les périodes de la journée: la nuit les équipes doivent se réveiller, de même les équipes en séance de sport doivent revenir jusqu'à la caserne; enfin les pauses et les changements d'équipes peuvent également avoir des conséquences sur les capacités de réactions des équipes.

\subsubsection{Prédiction de trajet}

Plusieurs paramètres ont été déjà calculés dans le jeu de données pour aider l'estimation des trajets. On a ainsi les données suivantes:
\begin{description}
	\item[GPS tracks departure-presentation] liste de positions GPS enregistrées par le véhicule
	\item[GPS tracks departure-presentation datetime] listes des dates associées à ces positions
	\item[OSRM estimated route] l’itinéraire recommandé entre les deux points
	\item[OSRM estimated distance] la distance associée à cet itinéraire
	\item[OSRM estimated duration] la durée associée à cet itinéraire
\end{description}

Les deux premières données sont difficiles à prendre en compte dans un entrainement pour plusieurs raisons. Tout d'abord seul 30\% du jeu de données contient ces informations, ce qui réduit la portée d'un éventuel apprentissage. Ensuite, la collecte des positions est irrégulière et donc extrêmement bruitée. Cependant on peut utiliser ces données en post-traitement. On pourrait vérifier que le temps prévu est cohérent avec les données GPS, c'est-à-dire qu'on prédit une arrivée après toutes les données temporelles correspondant au trajet. \\

Pour les prédictions OSMR, on essaie d'améliorer la prédiction en modifiant les paramètres d'OSMR, pour calculer un trajet adapté au type de véhicule utilisé. Une comparaison intéressante serait de voir quels sont les gains possibles en utilisant d'autres logiciels de prédictions de trajet. Globalement, on constate que les temps prédits sont beaucoup trop optimistes (de l'ordre de deux fois le temps de trajet réel, ce qui reflète la difficulté de circulation en région parisienne). On espère cependant qu'en indiquant le type de véhicule dans le prédiction de l'itinéraire, on puisse réduire l'imprécision de ces prédictions. Par exemple, savoir que le véhicule est un poids lourd exclut certaines routes et donc permet la prédiction d'un itinéraire plus réaliste. \\

On décide d'utiliser la prédiction du trajet pour en extraire uniquement une complexité du parcours, donnée par le nombre d'intersection dans l'itinéraire. L'idée sous-jacente est que ce sont avant tout les carrefours et les changements de directions qui demandent de la prudence et provoque de fort ralentissement dans l'itinéraire. Prendre en compte les estimations de temps et de distance d'itinéraire sont des options que l'on retient et que nous détaillons dans la section \ref{ss:osrm}.
