\section{Représentation utilisée : mise en forme du jeu et introduction d'information}

De plus, dans notre étude, nous n'avons pas uniquement utilisé les jeux de données issues du challenge, nous avons aussi utilisé d'autres sources pour augmenter notre représentation et fournir des informations pertinentes pour l'apprentissage de modèles statistique. De manière plus spécifique, nous utilisons dans notre étude:

\begin{enumerate}
	\item les données du traffic routiers de la ville de Paris ;
	\item les données météo sur l'année 2018 ;
	\item des nouvelles estimations d'itinéraires basées sur le type de véhicule.
\end{enumerate}

Dans cette section, nous présentons succintement les différents jeux de données et motivons leur usage dans notre étude. Cette section donne des références plus complètes pour l'accès et l'utilisation de ces sources externes.

\subsection{Adaptation du jeu de données du challenge}

Au vu des considérations présentées dans la partie précédente, on choisit de modifier la représentation de certaines variables pour leur donner plus de sens:

\begin{itemize}
	\item nous catégorisons les raisons d'alertes ;
	\item nous incluons les périodes de vacances et jours fériés dans l'analyse ;
	\item nous incluons une représentation circulaire pour replacer l'heure de chaque intervention dans la journée, les jour dans le mois, et les mois dans l'année.
\end{itemize}

De même, on choisit d'en introduire de nouvelles dérivées:

\begin{itemize}
	\item nous ajoutons des catégories de véhicules grâce aux informations données par la Brigade ;
	\item nous ajoutons le rang relatif de départ du véhicule pour chaque intervention.
\end{itemize}

Enfin, nous ne tenons pas compte des variables obtenues après interventions et des données disponibles pour un tiers des enregistrements, c'est-à-dire:

\begin{itemize}
	\item des positions GPS observées des véhicules
	\item de certaines informations concernant leur status avant le départ
\end{itemize}

\subsection{Ajout d'estimations des temps de trajets selon le type de véhicule}
\label{ss:osrm}

Premièrement, nous avons vu dans la première partie que le type de véhicule influence grandement les temps de transit des équipes : il est donc pertinent de prendre en compte cette information dans les prédictions. De plus, le jeu de données initial donne pour chaque sélection de véhicule d'intervention des estimations de la durée du trajet et sur la distance à parcourir. Ces estimations furent réalisées avec un serveur de routage \emph{Open Source Rooting Machine} \cite{osrm}, dont le code est ouvert et libre mais ne prend néanmoins pas en compte l'information du type de véhicule : ces estimations d'itinéraires furent réalisées avec le profil de base d'automobile d'OSRM. \\

Afin d'introduire d'informations notables du type de véhicules d'intervention, on choisit de recréer des estimations d'itinéraire qui prennent en compte deux types de véhicule : les véhicules légers et les véhicules lourds . \\

On réalise cela en trois étapes:
\begin{itemize}
	\item tout d'abord, on réalise deux profils pour les véhicules légers et pour les véhicules lors via une modélisation (dimensions, poids, pénalités de manœuvre) et une indication sur les voies que les véhicules peuvent emprunter ou non. On se réfèrera à \cite{osrm_profiles} pour plus de détails sur la réalisation de tels scripts ;
	\item on réalise la création de graphes pour des représentation adaptés, partitionnement du graphes, et customisation sur la zone géographiques d'intérêt. On se référera à \cite{osrm_graph} pour plus de détails sur ce point;
	\item on obtient les estimations sur les itinéraires grâce à l'interface REST. Les itinéraires sont construit grâce à l'algorithme multi-niveau de Dijstrka \cite{dijkstra1959note} ou l'algorithme des contractions hiérarchiques \cite{contraction}.
\end{itemize}

Nous avons mis en place deux serveurs pour chacun de ces profils ; ceux-ci sont disponibles en ligne ici:
\begin{itemize}
	\item \url{https://car.osrm.jjerphan.xyz}
	\item \url{https://heavy.osrm.jjerphan.xyz}
\end{itemize}

\subsubsection{Traitement des variables catégorielles}

Nous avons encodé les variables catégorielles restantes selon une représentation en \emph{one-hot-encoding}.

\subsection{Introduction d'estimations de la congestion du trafic routier}
\label{ss:traffic_data}

La présence ou non de congestion de trafic influe beaucoup sur la capacité des secours à intervenir. De plus, la ville de Paris met à disposition l'ensemble des données horaires de débit et taux d'occupation remontées par les capteurs permanents de trafic installés sur le réseau urbain et les boulevards périphériques pour les années 2014-2018  -- les données du trafic sont disponibles et explicitement présentées sur le site \emph{Paris, Data} \cite{sitedataparis} : nous introduisons donc dans notre étude des estimations de la congestion du trafic routier. Ces données sont également quasiment accessible en temps réel aujourd'hui, et pourrait donc être utilisé pour des prédictions en temps réel.\\

Ces données se décomposent en deux jeux de données: un jeu reprenant les informations du réseau routier (positions des capteurs sur les voies, etc.) et un autre reprenant les données de trafic sur ces mêmes voies. Ce dernier est scindé par semaine et possède une donnée par voie et par heure. Deux informations principales sont données:

\begin{enumerate}
	\item $q$, le débit (nombre de véhicules comptés pendant l’heure)
	\item $k$, le taux d’occupation (en pourcentage de temps d'occupation de la station de mesure par des véhicules sur l’heure).
\end{enumerate}

Pour une intervention donnée, on estime $k$ et $q$au point de départ et au point d'arrivée en moyennant les valeurs de ces indicateurs sur les cent plus proches nœuds du réseaux voisins à la même date. \\

Pour réaliser cela efficacement, on utilise des \emph{KDTree}, structures de données arborescentes à complexité optimale pour la recherche de points dans un espace euclidien, pour trouver de manière efficace les nœuds proches des points sur lesquels on veut réaliser l'estimation. Il faut noter que, pour des raisons de volumes de données, seules les données du trafic de 2018 ont été utilisées. Néanmoins, les outils d'analyse mis en place s'adaptent à l'introduction des données sur les années précédentes.



\subsection{Introduction du contexte météorologique}
\label{ss:meteo_data}

La météo est un paramètre qui influence directement le trafic et donc la réactivité des services de secours. \emph{Météo France} met à disposition de tous les données issues de 62 stations présentes sur l'ensemble du territoire Français. Dans notre étude, nous utilisons les données de la station d'Orly\footnote{De coordonnées GPS (48.7168,2.3843). C'est la station météorologique la plus proche de Paris} sur les descripteurs suivants:

\begin{itemize}
	\item \texttt{rafper} : Rafale sur une période (en $m.s^{-1}$) ;
	\item \texttt{t}: Température (en $K$) ;
	\item \texttt{u}: Humidité (en pourcentage) ;
	\item \texttt{ht\_neige}: Hauteur totale de la couche de neige, glace, autre au sol (en $m$) ;
	\item \texttt{rr12}: Précipitations (en $mm$);
	\item \texttt{vv}: Visibilité horizontale (en $m$).
\end{itemize}

La documentation et le téléchargement de ces données sont disponibles sur le site des données publiques de Météo France\cite{meteofrance}.

% \subsection{Données des centres de secours}

% Les données issues des centres de secours sont disponibles en ligne, sur la plateforme ouverte des données publiques françaises\cite{caserne}. Les principales informations intéressantes de ce jeu de données sont son groupement, sa dénomination ainsi que sa position géographique donnée par sa longitude et sa latitude. Ces données furent utiles pour l'analyse exploratoire des données.

% \subsection{Autres données}

% D'autres sources pourrait être utilisé dans un modèle plus complet. A priori, une grande partie des données peut être expliquée par les conditions de traffic. Pour cela, on peut utiliser des API tierces qui prennent en compte la circulation selon différents critères comme le poids du véhicule. La présence de voie de bus sur l'itinéraire peut aussi grandement faciliter l'arrivée des pompiers. Une façon de récupérer l'emplacement de ces voies pourraint être de passer par les itinéraires vélo, qui indiquent si l'on a une circulation en voie de bus. \\

% Une autre origine de ralentissement est la présence de manifestations, qui peuvent fermer des routes et augmenter la circulation dans un secteur donné. Les décalarations de manifestations sont disponibles sur le site de la préfecture, ainsi que sur plusieurs sites associatifs\footnote{Pour les arrêtés de la préfecture \url{https://www.prefecturedepolice.interieur.gouv.fr/Nous-connaitre/Documentation/Salle-de-presse/Communiques-de-presse/Archives-Manifestations}, pour un exemple de site associatif : \url{https://paris.demosphere.net/manifestations-paris}}. Cependant, ces données telles qu'elles sont fournies sont difficilement ajoutables à l'ensemble des données. \\

% Une autre source de variabilité est la météo du jour. Par exemple, en hiver, un jour de neige va immédiatement engendrer de grands problèmes de circulation, et donc un allongement du temps de parcours des véhicules de secours. Ces données sont disponibles et pourraient être ajoutées. \\

% Enfin, d'autres données concernant l'organisation des centres de secours pourrait être ajouté, telle l'heure des relèves, des pauses, des séances de sport ou le nombre de personnes sur place. Cependant, le risque d'inclure ce genre de données pourrait être de finalement discriminer d'individus à individus, ce qui pourrait à terme produire une pression supplémentaire sur les équipes non forcément justifiées et non forcément justifiables. Cela pourrait aussi introduire de nombreux biais dans la sélection, par exemple faire partir toujours les mêmes équipes sur certains types d'interventions, juste parce qu'elles ont eu par hasard de meilleurs données du à la variabilité naturelle des circonstances. Ceci renforceraient donc leur expérience, donc confirmerai ces biais, mais ne serait pas forcément bon sur le long terme : mauvaise répartition de la pénibilité des interventions, cercles vicieux d'évitements de ces interventions pour les autres équipes...