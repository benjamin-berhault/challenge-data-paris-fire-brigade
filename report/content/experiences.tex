\section{Expériences : modèles développés et comparaisons}

Lors de nos travaux, nous avons testé différentes représentations de données et plusieurs modèles. Nous nous sommes tout d'abord concentrés sur des modèles plus simples, des modèles linéaires, sur une représentation qui prenait en compte les connaissances \emph{a priori} présentées dans la section précédente. Nous nous sommes ensuite intéressés à des méthodes arborescentes connues comme performantes pour obtenir de bons résultats. \\

Cette partie expose les éléments les plus importants et décisifs des différentes expérimentations et résultats obtenus pour chacun de ces modèles. La section \ref{ss:lin} présente les modèles de régressions linéaires testés, la section \ref{ss:arbres} l'intérêt des méthodes arborescentes et la section \ref{ss:nn} notre positionnement vis-à-vis de modèles neuronnaux.  De même, la stratégie d'optimisation d’hyperparamètres des algorithmes est présentée dans la section \ref{ss:methode}. Enfin les résultats de nos modèles sont présentés dans la section \ref{ss:results} et des interprétations sont dréssées.

\subsection{Régression Linéaire}
\label{ss:lin}

Nous avons tout d'abord commencé à utiliser des régressions linéaires pour rendre compte des enrichissements successifs de la représentation, qui furent, dans l'ordre:

\begin{itemize}
	\item la mise en place de colonnes de catégorisation pour les variables catégorielles ;
	\item l'introduction de l'estimation local du trafic
	\item l'intégration du type de véhicule et catégorisation
	\item l'intégration du rang du véhicule et de la taille de la flotte
	\item l'introduction de nouvel estimation des itinéraires adaptés au véhicules (temps, distance et nombre d'intersection)
	\item l'intégration des vacances et jours fériés
	\item l'intégration des données issues de la météo
\end{itemize}

\subsection{Méthodes arborescentes}
\label{ss:arbres}

Les techniques arborescentes sont populaires pour apprendre des modèles. Celles-ci se basent sur des modèles plus simples qui sont les arbres de décision. L'algorithme des arbres de décision consiste à découper récursivement l'espace des descripteurs selon un critère de test sur un ou plusieurs descripteurs. Le résultat de l'apprentissage est donc une découpe de l'espace en hypercubes, qui relativement explicables en fonction de la taille de l'arbre obtenu. Néanmoins, ces modèles sont relativement simples et disposent d'une grande variance. Pour combler ce problème, on utilise des \emph{techniques arborescentes} qui se basent sur l'apprentissage de plusieurs arbres et sur la combinaison de leur prédictions. \\

Dans la suite, nous utilisons deux méthodes basées sur des techniques arborescentes: une technique classique de \emph{bagging}, les forêts aléatoires -- populairement nommées \emph{RandomForests} -- et une version plus récente des techniques de \emph{boosting} d'arbres, \emph{Gradient Boosting Decision Tree}, implémentés dans les algorithmes de la bibliothèque \emph{LightGBM} \cite{lightgbm}.

\subsubsection{Random Forest}

Cette technique d'apprentissage est tirée des arbres de décisions. Les random forest permettent de garder une meilleur capacité de généralisation en bootstrappant sur des arbres de décisions qui sélectionne des paramètres de façon aléatoire. \\

Nous utilisons l'implémentation de \emph{scikit-learn} \cite{scikit-learn}. Nous obtenons des scores respectables avec cette technique, alors même qu'elle n'est pas a priori la plus adaptée ici puisqu'on cherche à prédire des phénomènes variants continument et non pas des classifications.

\subsubsection{Gradient Boosting Decision Tree}

Le \emph{Gradient Boosting Decision Tree} popularisé avec la bibliothèque \emph{XGBoost} est une technique d'apprentissage de modèle très performante qui est utilisée dans beaucoup de compétitions. La bibliothèque plus récente, \emph{LightGBM} \cite{lightgbm}, ré-implémente certains de cette méthodes en proposant deux amélioration permettant un apprentissage plus rapide et moins coûteux en calculs.

\subsection{Réseaux de neurones}
\label{ss:nn}

Nous avons enfin entraîné des réseaux de neurones grâce à la bibliothèque \emph{Keras}. Ce challenge ne reposant pas sur des données d'images ou de signaux, nous nous sommes concentrés sur des architectures non convolutives avec des couches denses uniquement. Les résultats n'ont pas été à la hauteur des deux précédents algorithmes. De plus, nous avons préféré nous concentrer sur des modèles simples dont les résultats peuvent être interprétables et dont l'entraînement est rapide est peu couteux en calcul. Tous les entrainements ont été réalisé sur nos propres machines (cpu).

\subsection{Cycle d'apprentissage et d'évaluation}
\label{ss:methode}

Afin d'améliorer et d'évaluer les performances des modèles développés, nous mettons en place une recherche d'hyperparamètres et une évaluation des performances sur les métriques. Nous utilisons \emph{Optuna} \cite{optuna} pour la calibration d'hyperparamètres. \emph{Optuna} est une bibliothèque récente permettant d'accéder à heuristiques de recherches récentes avec une interface intuitive. Nous utilisons une validation croisée sur 3 \emph{folds} pour avoir une recherche non biaisée d'hyperparamètres. \\

Enfin, après mélange aléatoire du jeu de donnée, nous utilisons $80\%$ du jeu de données pour l'entraînement et $20\%$ du jeu de données pour la validation. \\

\subsection{Résultats et interprétations}
\label{ss:results}

Nous donnons ici nos résultats pour les modèles appris. De même, nous commentons notre score sur le jeu de données privé. Nous donnons enfin une interprétation rapide des modèles appris.

\subsubsection{Résultats sur le jeu de données public}

Les différents scores des modèles produits sont donnés dans le tableau \ref{results}. On remarque que les différentes hausses de scores correspondent à l'introduction des nouvelles données dans la représentation. \\

On interprète brièvement nos résultats grâce à la hausse ainsi:

\begin{itemize}
	\item une meilleure représentation du jeu de données du challenge permet une amélioration notable des performances ;
	\item les différents jeux de données introduits permettent d'améliorer le modèle ;
	\item la recherche d'hyperparamètres est importante pour que les algorithmes plus complexes puissent créer des modèles performants ;
	\item la recherche d'hyperparamètres permet d'obtenir une amélioration à la marge sur la fin  ;
\end{itemize}

\begin{table}[H]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	Rang & Date & Méthodes & Commentaire & Score Public ($R^2$) & Hausse Score \\
	\hline
	\hline
	 1 & 23 Mars  & 2 LightGBMRegr. & Météo + Rech. HP & 0.2373 & 0.00040
 \\
	 2 & 23 Mars  & 3 LightGBMRegr. & Metéo + Rech. HP & 0.2369 & \textbf{0.00600} \\
	 3 & 18 Mars  & 2 LightGBMRegr. & Recherche HP & 0.2309 & 0.00089 \\
	 4 & 17 Mars  & 2 LightGBMRegr. & Recherche HP & 0.2300 & 0.00070 \\
	 5 & 26 Fév.  & 2 LightGBMRegr.  & & 0.2293 & 0.00600 \\
	 6 & 21 Mars  & 2 LightGBMRegr. &  & 0.2233 & 0.00070 \\
	 7 & 19 Mars  & 2 Random Forests  &  & 0.2226 & 0.00070 \\
	 8 & 20 Mars  & 2 Random Forests &  & 0.2219 & 0.00299 \\
	 9 & 22 Fév.  & 2 LightGBMRegr.  &  & 0.2189 & 0.00460 \\
	10 & 20 Mars  & 2 Random Forests & Estim. OSRM & 0.2143 & \textbf{0.01285} \\
	11 & 21 Mars  & 2 Random Forests & Info véhicules & 0.20144 & \textbf{0.01684} \\
	12 & 20 Fév.  & 2 Random Forests & Recherche HP & 0.1846 & \textbf{0.02989} \\
	13 & 24 Fév.  & 2 Random Forests  & & 0.1547 & 0.00100 \\
	14 & 24 Fév.  & 2 LinearRegression  &  & 0.1537 & 0.00050 \\
	15 & 20 Fév.  & 2 LinearRegression  &  Categ. + Traffic & 0.1532 & \textbf{0.06100}\\
	16 & 8 Fév. & 2 LinearRegression & Données challenge & 0.0922 & 0.07840\\
	\hline
	17 & 4 Fév. & Soumission de la moyenne & 0 & 0.0138 & 0.02950\\
	18 & 3 Fév. & Soumission constante & 0 & -0.0157 & \\
	\hline
	\end{tabular}
	\caption{Récapitulatif des résultats}
	\label{results}
\end{table}

Avec une simple régression linéaire, nous arrivons à des scores raisonnables vis-à-vis du classement public avec, dans le meilleur cas, un coefficient de déterminations de $0.15$. Néanmoins, les modèles issus des méthodes arborescentes sont ceux qui donnent les meilleurs résultats.

\subsubsection{Score sur le jeu de données privé et interprétation}

Nous obtenons un coefficient de détermination de \textbf{0.3334} sur le jeu de données privé.
Ce score est associé à la soumission de meilleur score public au 19 Mars 2019 -- la soumission de rang 3 dans le tableau \ref{results}. \\

Entre le score public et ce score, il y a une hausse de $0.1$ soit une hausse de près d'un tiers du score. La majorité des équipes a obtenu également une hausse positive de leur score, malgré au moins une exception (figure \ref{publicprive}). Notre hausse fut la plus importante. \\

\begin{figure}[H]
	\centering
	\subfigure[]{\includegraphics[width=0.48\textwidth]{public}}
	\subfigure[]{\includegraphics[width=0.48\textwidth]{prive}}
	\caption{Score public et privé au 19 mars 2020 ; notre identifiant : \texttt{edwige \& jjerphan}}
	\label{publicprive}
\end{figure}

Sous les conditions de bonnes distributions des données dans le jeu d'évaluation privé, nous interprétons ce score comme le résultat de l'apprentissage d'un modèle qui généralise bien. Ce résultat nous conforte dans notre démarche et dans la création de notre représentation.

\subsubsection{Interprétation des modèles}

L'importance des variables peut-être étudiée pour juger de la pertinence de celles-ci. Pour les modèles entraînés avec \emph{LightGBM} et \emph{RandomForests}, cette information correspond à la sollicitation d'une variable de séparations sur les nœuds des arbres de décision construits. Les figures \ref{lgbm_prep} et \ref{lgbm_transit} donnent l'importance des variables pour les deux modèles créés avec \texttt{LightGBMRegressor}. De même, les figures \ref{rf_prep} et \ref{rf_transit} donnent l'importance des variables pour les deux modèles obtenus avec \texttt{RandomForestRegressor}. \\

Pour les modèles réalisés avec \texttt{LightGBM}, on remarque que les variables qui influencent les résultats sont principalement les variables correspondant à la position géographique des lieux de départ et d'intervention ainsi que la distance et durée estimée par les itinéraires OSRM. Après ces variables, les estimations de la congestion de trafic puis certaines variables des conditions météorologiques viennent contribuer au résultat. De plus, parmi les raisons d'alerte, on note que celle se rapportant aux incendies (\texttt{raison\_feu}) est celle avec l'importance la plus probante. Les variables catégorielles binaires sont généralement celles qui influent le moins sur le résultat final.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{lgbm_prep}
	\caption{Importance des variables sur le temps de préparation pour \texttt{LightGBMRegressor}}
	\label{lgbm_prep}
\end{figure}


\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{lgbm_transit}
	\caption{Importance des variables sur le temps de transit pour \texttt{LightGBMRegressor}}
	\label{lgbm_transit}
\end{figure}

Les résultats observés pour les modèles obtenus avec \texttt{RandomForestRegressor} sont semblables en terme de classement des variables par leur importance. On notera cependant que seules quelques variables semblent influencer majoritairement les résultats dans le cas de ces modèles. De plus, pour le modèle associé au temps de préparation, les variables \texttt{minute\_in\_day\_sin} et \texttt{minute\_in\_day\_cos} sont les deux qui influencent de manière notable les prédictions. Ce résultat s'interprète facilement: la période de la journée influence grandement le temps de préparation. Encore une fois, la majorité des variables binaires influence peu les résultats finaux de ces deux modèles.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{rf_prep}
	\caption{Importance des variables sur le temps de préparation pour \texttt{RandomForestRegressor}}
	\label{rf_prep}
\end{figure}


\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{rf_transit}
	\caption{Importance des variables sur le temps de transit pour \texttt{RandomForestRegressor}}
	\label{rf_transit}
\end{figure}